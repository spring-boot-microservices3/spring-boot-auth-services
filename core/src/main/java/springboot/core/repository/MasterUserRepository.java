package springboot.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import springboot.core.model.entity.MasterUser;

import java.util.List;

/**
 * Created by f.putra on 9/8/20.
 */
public interface MasterUserRepository extends BaseRepository<MasterUser, String> {

    @Query("SELECT u FROM MasterUser u WHERE u.userId = :id")
    MasterUser getUser(@Param("id") String userId);

    @Query("SELECT u FROM MasterUser u WHERE u.username = :username")
    MasterUser getByUsername(@Param("username") String username);

    Boolean existsByUsername(String username);

    Boolean existsByUsernameOrEmail(String username, String email);

    List<MasterUser> findByLastLoginAtIsNotNull();
}
