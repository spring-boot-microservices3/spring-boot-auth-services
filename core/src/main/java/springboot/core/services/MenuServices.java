package springboot.core.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import springboot.core.common.*;
import springboot.core.model.dto.ActivityGenericLog;
import springboot.core.model.dto.MenuDTO;
import springboot.core.model.dto.response.MenuResponse;
import springboot.core.model.entity.MasterMenu;
import springboot.core.model.mapper.MenuMapper;
import springboot.core.repository.MasterMenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by f.putra on 9/24/20.
 */
@Service
public class MenuServices extends BaseCriteriaBuilder {
    @Autowired
    MasterMenuRepository masterMenuRepository;
    @Autowired
    ResourceBundle resourceBundle;
    @Autowired
    LogServices logServices;

    @Transactional
    public MasterMenu saveMenu(MenuDTO payloadMenu) {
        if (masterMenuRepository.existsByNameIgnoreCase(payloadMenu.getName().trim().toLowerCase()))
            throw new IllegalArgumentException(resourceBundle.getMessage("err.menu.validation.data_existed"));

        if (masterMenuRepository.existsMasterMenuByPath(payloadMenu.getPath().trim().toLowerCase()))
            throw new IllegalArgumentException(resourceBundle.getMessage("err.menu.validation.data_existed"));

        MasterMenu masterMenu = new MasterMenu();
        masterMenu.setName(payloadMenu.getName().trim());
        masterMenu.setIcon(payloadMenu.getIcon());
        masterMenu.setParent(payloadMenu.getParent().isEmpty() ? "0" : payloadMenu.getParent());
        masterMenu.setPath(payloadMenu.getPath());
        masterMenu.setState(payloadMenu.getState());
        masterMenu.setLayout("/admin");
        masterMenu.setComponent(payloadMenu.getPath().equals("") ? "" : this.menuHelper(payloadMenu.getPath().toUpperCase()));
        masterMenu.setOrderMenu(payloadMenu.getOrder());

        masterMenu = masterMenuRepository.save(masterMenu);
        updateParentMenu(masterMenu.getParent());
        return masterMenu;
    }

    private void updateParentMenu(String parentId) {
        if (!parentId.equals("0")) {
            MasterMenu menu = masterMenuRepository.findById(parentId).get();
            menu.setCollapse(true);
            masterMenuRepository.save(menu);
        }
    }

    @Transactional(readOnly = true)
    public MasterMenu getMenuById(String MenuId) {
            return masterMenuRepository.findById(MenuId).orElseThrow();
    }

    @Transactional(readOnly = true)
    public Page<MenuResponse> findAllMenu(Pageable pageable, MenuDTO menuDTO) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MasterMenu> query = builder.createQuery(MasterMenu.class);
        Root<MasterMenu> root = query.from(MasterMenu.class);
        root.alias("menuId");
        applyOrderByCreatedAndUpdateDate(builder, root, query);

        List<Predicate> predicates = new ArrayList<>();
        GenericSpesification<MasterMenu> genericSpesification = new GenericSpesification<>();
        if (menuDTO.getName() != null && !StringUtils.isBlank(menuDTO.getName())) {
            predicates.add(builder.like(builder.lower(root.get("name")), "%".concat(menuDTO.getName().toLowerCase()).concat("%")));
            genericSpesification.add(new SearchCriteria(root.get("name"), menuDTO.getName().toLowerCase(), SearchOperation.MATCH));
        }
        if (menuDTO.getPath() != null && !StringUtils.isBlank(menuDTO.getPath())) {
            predicates.add(builder.equal(root.get("path"), menuDTO.getPath()));
            genericSpesification.add(new SearchCriteria(root.get("path"), menuDTO.getPath(), SearchOperation.EQUAL));
        }
        if (menuDTO.getParent() != null && !StringUtils.isBlank(menuDTO.getParent())) {
            predicates.add(builder.equal(root.get("parent"), menuDTO.getParent()));
            genericSpesification.add(new SearchCriteria(root.get("parent"), menuDTO.getParent(), SearchOperation.EQUAL));
        }
        if (menuDTO.getIsDelete() != null) {
            predicates.add(builder.equal(root.get("isDelete"), menuDTO.getIsDelete()));
            genericSpesification.add(new SearchCriteria(root.get("isDelete"), menuDTO.getIsDelete(), SearchOperation.EQUAL));
        }

        // for getting parent menu only
        if (menuDTO.getCollapse() != null) {
            predicates.add(builder.or(
                    builder.equal(root.get("collapse"), menuDTO.getCollapse()),
                    builder.equal(root.get("parent"), "0")
            ));
        }
        query.where(builder.and(predicates.toArray(new Predicate[0])));

        TypedQuery<MasterMenu> queryList = entityManager.createQuery(query)
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize());

        long totalRows = masterMenuRepository.count(genericSpesification);

        List<MasterMenu> menus = queryList.getResultList();
        List<MenuResponse> responses = MenuMapper.INSTANCE.toResponseList(menus);
        for (MenuResponse dto : responses) {
            if (!dto.getParent().equalsIgnoreCase("0")) {
                dto.setParentName(masterMenuRepository.findById(dto.getParent()).get().getName());
            }
        }
        return new PageImpl<>(responses, pageable, totalRows);
    }

    public boolean deleteMenu(String menuId) {
        try {
            MasterMenu menu = masterMenuRepository.findById(menuId).get();
//            menu.setDelete(true);
            masterMenuRepository.save(menu);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Transactional
    public MasterMenu updateMenu(MenuDTO payloadMenu) throws JsonProcessingException {
        MasterMenu masterMenu = getMenuById(payloadMenu.getId());
        logServices.saveGeneralActivityLog(new ActivityGenericLog(masterMenu, payloadMenu, "Master Menu"));
        if (masterMenu != null) {
            masterMenu.setName(payloadMenu.getName());
            masterMenu.setIcon(payloadMenu.getIcon());
            masterMenu.setParent(payloadMenu.getParent());
            masterMenu.setState(payloadMenu.getState());
            masterMenu.setPath(payloadMenu.getPath());
            masterMenu.setOrderMenu(payloadMenu.getOrder());
//            masterMenu.setDelete(payloadMenu.getIsDelete());

            try {
                masterMenu = masterMenuRepository.save(masterMenu);
                updateParentMenu(masterMenu.getParent());
                return masterMenu;
            } catch (DataIntegrityViolationException e) {
                throw new IllegalArgumentException(resourceBundle.getMessage("err.menu.validation.data_existed"));
            }
        }
        return null;
    }

    public Set<MasterMenu> searchMenu(String name) {
        return masterMenuRepository.searchMenuByName(name);
    }

    public String menuHelper(String initial) {
        switch (initial) {
            case "/MENU":
                return "Menu";
            case "/ROLE":
                return "Role";
            case "/USER":
                return "User";
            case "/SYNC":
                return "Sync";
            case "/ROLEACCESS":
                return "RoleAccess";
            case "/LOGIN":
                return "LoginPage";
            case "/REGISTER":
                return "RegisterPage";
            case "/REGION":
                return "Region";
            case "/COMPANY":
                return "Company";
            case "/ESTATE":
                return "Estate";
            case "/AFDELING":
                return "Afdeling";
            case "/BLOCK":
                return "Blok";
            case "/EMPLOYEE":
                return "Employee";
            case "/NONEMPLOYEE":
                return "NonEmployee";
            case "/LANDUSE":
                return "Landuse";
            case "/MANDOR":
                return "Mandor";
            case "/ACTIVITY":
                return "Activity";
            case "/BIAYA":
                return "Biaya";
            case "/KASSAN":
                return "Kassan";
            case "/PANENVARIABLE":
                return "PanenVariable";
            case "/PUPUKSUB":
                return "PupukSub";
            case "/PUPUKTBM":
                return "PupukTbm";
            case "/PUPUKTM":
                return "PupukTm";
            case "/PRODUKTIVITASPANEN":
                return "ProduktivitasPanen";
            case "/RAWAT":
                return "Rawat";
            case "/PANEN":
                return "Panen";
            case "/PUPUK":
                return "Pupuk";
            case "/GLACCOUNT":
                return "GlAccount";
            case "/PAYROLL":
                return "Payroll";
            case "/VRA":
                return "Vra";
            case "/NEXT":
                return "NextSprint";
            case "/ABOUT":
                return "About";
            default:
                return null;
        }
    }
}
