package springboot.rest.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springboot.core.model.dto.AddRoleDTO;
import springboot.core.model.dto.RoleAccessDTO;
import springboot.core.services.MenuRoleServices;

import java.util.List;

/**
 * Created by f.putra on 10/7/20.
 */
@RestController
@RequestMapping("access")
public class RoleAccessController extends BaseController {

    @Autowired
    MenuRoleServices menuRoleServices;

    @ApiOperation(value = "Add access for Role", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("/add-access")
    public ResponseEntity<?> addMenuRole(@RequestBody AddRoleDTO v) {
        try {
            return getCallBack(menuRoleServices.createMenuRole(v), HttpStatus.OK);
        } catch (Exception e) {
            return getCallBack(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/list")
    public ResponseEntity<?> findAllRoleAccess(@RequestParam(name = "offset") Integer offset,
                                               @RequestParam(name = "limit") Integer limit,
                                               @RequestParam(name = "roleName", required = false) String roleName,
                                               @RequestParam(name = "menuName", required = false) String menuName,
                                               @RequestParam(name = "level", required = false) Integer level) {
        return getCallBack(menuRoleServices.findAllMenuRole(PageRequest.of(offset, limit), roleName, level, menuName), HttpStatus.OK);
    }

    @ApiOperation(value = "Update access for Role", produces = MediaType.APPLICATION_JSON_VALUE)
    @PutMapping("/update-access")
    public ResponseEntity<?> updateMenuRole(@RequestBody List<RoleAccessDTO> dtoList) {
        try {
            return getCallBack(menuRoleServices.updateRoleAccess(dtoList), HttpStatus.OK);
        } catch (Exception e) {
            return getCallBack(e, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Delete access for Role", produces = MediaType.APPLICATION_JSON_VALUE)
    @DeleteMapping("/delete-access")
    public ResponseEntity<?> deleteMenuRole(@RequestBody List<RoleAccessDTO> dtoList) {
        return getCallBack(menuRoleServices.deleteRoleAccess(dtoList), HttpStatus.OK);
    }
}
