package springboot.core.common;

import javax.persistence.criteria.Path;

/**
 * Created by f.putra on 10/20/20.
 */
public class SearchCriteria {

    private Path key;
    private Object value;
    private SearchOperation operation;

    public SearchCriteria(Path key, Object value, SearchOperation operation) {
        this.key = key;
        this.value = value;
        this.operation = operation;
    }

    public Path getKey() {
        return key;
    }

    public void setKey(Path key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public SearchOperation getOperation() {
        return operation;
    }

    public void setOperation(SearchOperation operation) {
        this.operation = operation;
    }
}
