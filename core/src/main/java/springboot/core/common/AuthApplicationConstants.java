package springboot.core.common;

public class AuthApplicationConstants {

  private AuthApplicationConstants(){}


  //Header
  public static final String HEADER_DEVICE_ID = "Device-Id";
  public static final String HEADER_DEVICE_NAME = "Device-Name";
  public static final String HEADER_DEVICE_TYPE = "Device-Type";

  //Auth param
  public static final String DEVICE_ID = "device_id";
  public static final String DEVICE_TYPE = "device_type";
  public static final String DEVICE_NAME = "device_name";
  public static final String CLIENT_ID = "client_id";
  public static final String CLIENT_TYPE = "client_type";
  public static final String CLIENT_TENANT = "client_tenant";
  public static final String CLIENT_PLATFORM = "client_platform";

  public static final String SERVICE_HOME = "/";

  public static final String SERVICE_AUTH = "/";
  public static final String SERVICE_LOGIN = "/login";
  public static final String SERVICE_REGISTER = "/register";


}
