## SPRING BOOT SERVICE AUTHENTICATION AND AUTHORIZATION

- MENU TREE
- ROLE
- USER
#####
USER HAVE ROLE AND ROLE HAVE MENU
#####
```
--- EXAMPLE ---
- USER JHONE DOE
-- ROLE SUPER ADMIN
--- MENU *** 

```

####
HOW TO RUN
- first run `spring-boot-config-server`
- second `setup datasource on your database`
```
app  profile label       key                             value
auth	dev	latest	spring.primary.datasource.password	root
auth	dev	latest	spring.primary.datasource.username	root
auth	dev	latest	spring.primary.datasource.driver	org.postgresql.Driver
auth	dev	latest	spring.primary.datasource.url	jdbc:postgresql://localhost:5432/development
```
