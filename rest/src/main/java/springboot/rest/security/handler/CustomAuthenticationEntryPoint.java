package springboot.rest.security.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.bind.annotation.ControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by f.putra on 14/02/18.
 */
@ControllerAdvice
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {
    private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationEntryPoint.class);

    private static final long serialVersionUID = -8970718410437077606L;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        logger.info("Inside Rest Authentication entry Points");
        String error = "{ \"status\":\"FAILURE\",\"error\":{\"code\":\"401\",\"message\":\"" + authException.getMessage() + "\"} }";
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json");

        if (authException instanceof BadCredentialsException) {
            response.getOutputStream().println("{ \"Bad credential\": \"" + authException.getMessage() + "\" }");
        }
        if (authException instanceof AuthenticationCredentialsNotFoundException) {
            logger.info("Inside AuthenticationCredentialsNotFoundException");
            error = "{ \"status\":\"FAILURE\",\"error\":{\"code\":\"" + "TOKEN_EXPIRED" + "\",\"message\":\"" + "TOKEN_EXPIRED" + "\"} }";
        }
        response.getOutputStream().println(error);
    }
}
