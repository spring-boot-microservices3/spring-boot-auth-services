package springboot.core.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import springboot.core.common.*;
import springboot.core.model.dto.ActivityGenericLog;
import springboot.core.model.dto.RoleDTO;
import springboot.core.model.entity.MasterRole;
import springboot.core.repository.MasterRoleRepository;
import springboot.core.repository.MenuRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by f.putra on 9/24/20.
 */
@Service
public class RoleServices extends BaseCriteriaBuilder {

    @Autowired
    MasterRoleRepository masterRoleRepository;

    @Autowired
    MenuRoleRepository menuRoleRepository;

    @Autowired
    ResourceBundle resourceBundle;
    @Autowired
    LogServices logServices;

    @Transactional
    public MasterRole saveRole(RoleDTO payloadRole) {
        MasterRole existRole = masterRoleRepository.findByRoleNameIgnoreCase(payloadRole.getName().trim().toLowerCase());
        if (existRole != null)
            throw new IllegalArgumentException(resourceBundle.getMessage("err.role.validation.data_existed"));

        MasterRole masterRole = new MasterRole();
        masterRole.setRoleName(payloadRole.getName().trim().toUpperCase());
        masterRole.setLevel(payloadRole.getLevel());
        masterRole.setDescription(payloadRole.getDescription());
//        masterRole.setDelete(false);
        return masterRoleRepository.save(masterRole);
    }

    @Transactional
    public MasterRole updateRole(RoleDTO payloadRole) throws JsonProcessingException {
        MasterRole masterRole = masterRoleRepository.findById(payloadRole.getId()).orElseThrow();
        MasterRole existRole = masterRoleRepository.findByRoleNameIgnoreCase(payloadRole.getName().trim().toLowerCase());
        if (existRole != null) {
            if (!masterRole.getRoleId().equals(existRole.getRoleId()))
                throw new IllegalArgumentException(resourceBundle.getMessage("err.role.validation.data_existed"));
        }
        logServices.saveGeneralActivityLog(new ActivityGenericLog(masterRole, payloadRole, "Role"));
        masterRole.setRoleName(payloadRole.getName().toUpperCase());
        masterRole.setLevel(payloadRole.getLevel());
        masterRole.setDescription(payloadRole.getDescription());
//        masterRole.setDelete(payloadRole.getIsDelete());
        return masterRoleRepository.save(masterRole);
    }

    @Transactional(readOnly = true)
    public MasterRole getRoleById(String roleId) {
        return masterRoleRepository.getOne(roleId);
    }

    @Transactional(readOnly = true)
    public Page<MasterRole> findAllRole(Pageable pageable, RoleDTO v) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MasterRole> query = builder.createQuery(MasterRole.class);
        Root<MasterRole> rootMasterRole = query.from(MasterRole.class);
        applyOrderByCreatedAndUpdateDate(builder, rootMasterRole, query);

        List<Predicate> criteriaList = new ArrayList<Predicate>();
        GenericSpesification<MasterRole> genericSpesification = new GenericSpesification<>();
        if (!StringUtils.isBlank(v.getName())) {
            criteriaList.add(builder.or(builder.like(rootMasterRole.get("roleName"), "%" + v.getName().toUpperCase() + "%")));
            genericSpesification.add(new SearchCriteria(rootMasterRole.get("roleName"), "%" + v.getName().toUpperCase() + "%", SearchOperation.MATCH));
        }
        if (v.getLevel() != null) {
            criteriaList.add(builder.or(builder.like(rootMasterRole.<String>get("level").as(String.class), "%" + v.getLevel().toString() + "%")));
            genericSpesification.add(new SearchCriteria(rootMasterRole.<String>get("level"), v.getLevel(), SearchOperation.MATCH_INTEGER));
        }

        if (v.getIsDelete() != null) {
            criteriaList.add(builder.or(builder.equal(rootMasterRole.get("isDelete"), v.getIsDelete())));
            genericSpesification.add(new SearchCriteria(rootMasterRole.get("isDelete"), v.getIsDelete(), SearchOperation.EQUAL));
        }
        query.where(builder.and(criteriaList.toArray(new Predicate[]{})));
        TypedQuery<MasterRole> queryList = entityManager.createQuery(query)
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize());

        long totalRows = masterRoleRepository.count(genericSpesification);

        try {
            return new PageImpl<>(queryList.getResultList(), pageable, totalRows);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public boolean deleteRole(String roleId) {
        try {
            masterRoleRepository.deleteById(roleId);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public MasterRole update(String id, RoleDTO payloadRole) throws JsonProcessingException {
        MasterRole masterRole = getRoleById(id);
        logServices.saveGeneralActivityLog(new ActivityGenericLog(masterRole, payloadRole, "Role"));
        if (masterRole != null) {
            masterRole.setRoleName(StringUtils.isBlank(payloadRole.getName()) ? payloadRole.getName().trim().toUpperCase() : masterRole.getRoleName());
            masterRole.setLevel(payloadRole.getLevel());
            try {
                masterRole = masterRoleRepository.save(masterRole);
//                updateRoleMenu(masterRole.getRoleId(), payloadRole.getMenuIds());
                return masterRole;
            } catch (DataIntegrityViolationException e) {
                throw new IllegalArgumentException(resourceBundle.getMessage("err.role.validation.data_existed"));
            }
        }
        return null;
    }

    public boolean existsByRoleName(String roleName, Integer level) {
        return masterRoleRepository.existsByRoleNameIgnoreCaseOrLevel(roleName, level);
    }

    public List<MasterRole> searchRole(String name) {
        return masterRoleRepository.findRoleListByName(name);
    }
}
