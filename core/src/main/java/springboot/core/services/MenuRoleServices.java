package springboot.core.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import springboot.core.common.*;
import springboot.core.model.dto.ActivityGenericLog;
import springboot.core.model.dto.AddRoleDTO;
import springboot.core.model.dto.RoleAccessDTO;
import springboot.core.model.entity.MasterMenu;
import springboot.core.model.entity.MasterRole;
import springboot.core.model.entity.MenuRole;
import springboot.core.model.mapper.MenuRoleMapper;
import springboot.core.repository.MasterMenuRepository;
import springboot.core.repository.MasterRoleRepository;
import springboot.core.repository.MenuRoleRepository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by f.putra on 10/7/20.
 */
@Service
public class MenuRoleServices extends BaseCriteriaBuilder {
    @Autowired
    MenuRoleRepository menuRoleRepository;
    @Autowired
    MasterMenuRepository masterMenuRepository;
    @Autowired
    MasterRoleRepository masterRoleRepository;
    @Autowired
    ResourceBundle resourceBundle;
    @Autowired
    LogServices logServices;

    @Transactional
    public boolean createMenuRole(AddRoleDTO v) {
        try {
            saveRoleMenu(v.getRoleId(), v.getMenus());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Transactional(readOnly = true)
    public Page<RoleAccessDTO> findAllMenuRole(Pageable pageable, String roleName, Integer level, String menuName) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<MenuRole> query = cb.createQuery(MenuRole.class);
        Root<MenuRole> root = query.from(MenuRole.class);

        Join<MenuRole, MasterRole> role = root.join("role", JoinType.INNER);
//        query.select(root).orderBy(cb.desc(root.get("createdDtm")), cb.desc(root.get("updateDtm")));
        applyOrderByCreatedAndUpdateDate(cb, root, query);

        List<Predicate> predicates = new ArrayList<>();
        GenericSpesification<MenuRole> genericSpesification = new GenericSpesification<>();
        if (!StringUtils.isBlank(roleName)) {
            predicates.add(cb.like(cb.upper(role.get("roleName")), "%".concat(roleName.toUpperCase()).concat("%")));
            genericSpesification.add(new SearchCriteria(root.get("role").get("roleName"), roleName.toUpperCase(), SearchOperation.MATCH));
        }
        if (!StringUtils.isBlank(menuName)) {
            predicates.add(cb.like(cb.upper(root.get("menu").get("name")), "%".concat(menuName.toUpperCase()).concat("%")));
            genericSpesification.add(new SearchCriteria(root.get("menu").get("name"), menuName.toUpperCase(), SearchOperation.MATCH));
        }
        if (level != null) {
            predicates.add(cb.like(role.get("level").as(String.class), "%".concat(level.toString()).concat("%")));
            genericSpesification.add(new SearchCriteria(root.get("role").get("level"), level, SearchOperation.EQUAL));
        }

        query.where(cb.and(predicates.toArray(new Predicate[0])));

        TypedQuery<MenuRole> queryList = entityManager.createQuery(query)
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize());

        long totalRows = menuRoleRepository.count(genericSpesification);

        List<RoleAccessDTO> accessDTOList = updateParentName(MenuRoleMapper.INSTANCE.toDTOList(queryList.getResultList()));

        return new PageImpl<>(accessDTOList, pageable, totalRows);
    }

    @Transactional
    public void saveRoleMenu(List<String> roleId, List<String> menuIds) {
        // create or update role for new menu/ existing menu
        for (String role : roleId) {
            MasterRole masterRole = masterRoleRepository.getOne(role);
            for (String menuId : menuIds) {
                MenuRole menuRole = menuRoleRepository.findByMenuIdAndRoleId(menuId, masterRole.getRoleId());
                if (menuRole == null) {
                    menuRole = new MenuRole();
                    menuRole.setCreatedDate(new Date());
                    menuRole.setCreatedBy(ContextHolder.getRequestAuthor() != null ? ContextHolder.getRequestAuthor().getUsername() : "System");
                } else {
                    menuRole.setLastModifiedDate(new Date());
                    menuRole.setLastModifiedBy(ContextHolder.getRequestAuthor() != null ? ContextHolder.getRequestAuthor().getUsername() : "System");
                }

                MasterMenu menu = masterMenuRepository.getOne(menuId);
                menuRole.setMenuId(menuId);
                menuRole.setRoleId(masterRole.getRoleId());
                menuRole.setCreate(!menu.getParent().equals("0"));
                menuRole.setRead(!menu.getParent().equals("0"));
                menuRole.setUpdate(!menu.getParent().equals("0"));
                menuRole.setDelete(!menu.getParent().equals("0"));
                menuRole.setActive(true);

                try {
                    logServices.saveGeneralActivityLog(new ActivityGenericLog(null, menuRole, "SAVE ROLE ACCESS"));
                    menuRoleRepository.save(menuRole);
                } catch (Exception e) {
                    throw new RestClientException(resourceBundle.getMessage("err.data.existed"));
                }
            }
        }
    }

    @Transactional(readOnly = true)
    public List<MenuRole> getAllByRole(String roleId) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<MenuRole> query = cb.createQuery(MenuRole.class);
        Root<MenuRole> root = query.from(MenuRole.class);

        Join<MenuRole, MasterRole> role = root.join("role", JoinType.INNER);
        applyOrderByCreatedAndUpdateDate(cb, root, query);

        List<Predicate> predicates = new ArrayList<>();
        if (roleId != null) {
            predicates.add(cb.equal(role.get("roleId"), roleId));
        }
        predicates.add(cb.equal(root.get("isActive"), true));
        query.where(cb.and(predicates.toArray(new Predicate[0])));
        TypedQuery<MenuRole> queryList = entityManager.createQuery(query);

        return queryList.getResultList();
    }

    @Transactional(readOnly = true)
    public List<MenuRole> getAllMenuParentByRole(String roleId) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<MenuRole> query = cb.createQuery(MenuRole.class);
        Root<MenuRole> root = query.from(MenuRole.class);

        Join<MenuRole, MasterRole> role = root.join("role", JoinType.INNER);
        applyOrderByCreatedAndUpdateDate(cb, root, query);

        List<Predicate> predicates = new ArrayList<>();
        if (roleId != null) {
            predicates.add(cb.equal(role.get("roleId"), roleId));
            predicates.add(cb.equal(root.get("menu").get("parent"), "0"));
        }
        predicates.add(cb.equal(root.get("isActive"), true));
        query.where(cb.and(predicates.toArray(new Predicate[0])));
        TypedQuery<MenuRole> queryList = entityManager.createQuery(query);

        return queryList.getResultList();
    }

    public List<RoleAccessDTO> updateRoleAccess(List<RoleAccessDTO> list) throws JsonProcessingException {
        List<MenuRole> roles = new ArrayList<>();
        for (RoleAccessDTO dto : list) {
            try {
                MenuRole menuRole = menuRoleRepository.findById(dto.getMenu_role_id()).orElseThrow();
                logServices.saveGeneralActivityLog(new ActivityGenericLog(menuRole, dto, "Menu Role"));
                menuRole.setLastModifiedDate(new Date());
                menuRole.setLastModifiedBy(ContextHolder.getRequestAuthor() != null ? ContextHolder.getRequestAuthor().getUsername() : "System");
                menuRole.setCreate(dto.getIs_create());
                menuRole.setRead(dto.getIs_read());
                menuRole.setUpdate(dto.getIs_update());
                menuRole.setDelete(dto.getIs_delete());
                menuRole.setActive(dto.getIs_active());
                menuRole.setMenuId(dto.getMenu_id());

                MenuRole existingMenuRole = menuRoleRepository.findByMenuIdAndRoleId(dto.getMenu_id(), dto.getRole_id());
                if (existingMenuRole != null && !existingMenuRole.getMenuRoleId().equals(menuRole.getMenuRoleId()))
                    throw new RestClientException(resourceBundle.getMessage("err.data.existed"));

                roles.add(menuRoleRepository.save(menuRole));

                updateChildMenu(menuRole.getMenuId(), menuRole.getRoleId(), dto.getIs_active());
            } catch (DataIntegrityViolationException e) {
                throw new IllegalArgumentException(resourceBundle.getMessage("err.data.existed"));
            }
        }

        return updateParentName(MenuRoleMapper.INSTANCE.toDTOList(roles));
    }

    private void updateChildMenu(String parent, String roleId, boolean isActive) {
        List<MenuRole> menuRoles = menuRoleRepository.findMenuByRole(roleId);
        for (MenuRole menuRole : menuRoles) {
            if (menuRole.getMenu().getParent().equals(parent)) {
                menuRole.setActive(isActive);
            }
        }
        menuRoleRepository.saveAll(menuRoles);
    }

    public boolean deleteRoleAccess(List<RoleAccessDTO> list) {
        for (RoleAccessDTO dto : list) {
            MenuRole menuRole = menuRoleRepository.findById(dto.getMenu_id()).get();
            menuRole.setLastModifiedDate(new Date());
            menuRole.setLastModifiedBy(ContextHolder.getRequestAuthor() != null ? ContextHolder.getRequestAuthor().getUsername() : "System");
            menuRole.setActive(false);
            menuRole.setDelete(true);
            menuRoleRepository.save(menuRole);
        }

        return true;
    }

    @Transactional(readOnly = true)
    public List<RoleAccessDTO> updateParentName(List<RoleAccessDTO> dtoList) {
        for (RoleAccessDTO dto : dtoList) {
            Optional<MasterMenu> optional = masterMenuRepository.findById(dto.getParent());
            optional.ifPresent(menu -> dto.setParent_name(menu.getName()));
        }
        return dtoList;
    }
}
