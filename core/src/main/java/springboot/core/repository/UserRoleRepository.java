package springboot.core.repository;

import springboot.core.model.entity.UserRole;

/**
 * Created by f.putra on 9/8/20.
 */
public interface UserRoleRepository extends BaseRepository<UserRole, String> {
    UserRole findByUserId(String userId);
}
