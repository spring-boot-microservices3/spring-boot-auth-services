package springboot.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import springboot.core.model.entity.MasterRole;

import java.util.List;

/**
 * Created by f.putra on 9/24/20.
 */
public interface MasterRoleRepository extends BaseRepository<MasterRole, String> {

    MasterRole findByRoleName(String name);

    @Query("SELECT mr FROM MasterRole mr WHERE mr.roleName LIKE %:name%")
    List<MasterRole> findRoleListByName(@Param("name") String name);

    MasterRole findByRoleNameIgnoreCase(String roleName);

    Boolean existsByRoleNameIgnoreCaseOrLevel(String roleName, Integer level);
}
