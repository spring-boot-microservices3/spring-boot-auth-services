package springboot.core.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import springboot.core.model.entity.MasterMenu;
import springboot.core.model.dto.response.MenuResponse;

import java.util.List;

@Mapper
public interface MenuMapper {
    MenuMapper INSTANCE = Mappers.getMapper(MenuMapper.class);

    MenuResponse toResponse(MasterMenu menu);

    List<MenuResponse> toResponseList(List<MasterMenu> menu);
}
