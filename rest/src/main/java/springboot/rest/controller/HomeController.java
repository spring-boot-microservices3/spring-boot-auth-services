package springboot.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.rest.App;

@RestController
public class HomeController extends BaseController {

    @GetMapping("/")
    public ResponseEntity<?> index() {
        App.getLogger(this).info("Call Home API");
        return getCallBack("HELLO", HttpStatus.OK);
    }
}
