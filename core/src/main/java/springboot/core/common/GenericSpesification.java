package springboot.core.common;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by f.putra on 10/20/20.
 */
public class GenericSpesification<T> implements Specification<T> {

    private static final long serialVersionUID = -7119865246894668987L;
    private List<SearchCriteria> list;

    public GenericSpesification() {
        this.list = new ArrayList<>();
    }

    public void add(SearchCriteria criteria) {
        list.add(criteria);
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        //create a new predicate list
        List<Predicate> predicates = new ArrayList<>();

        //add add criteria to predicates
        for (SearchCriteria criteria : list) {
            if (criteria.getOperation().equals(SearchOperation.GREATER_THAN)) {
                predicates.add(builder.or(builder.greaterThan(criteria.getKey(), criteria.getValue().toString())));
            } else if (criteria.getOperation().equals(SearchOperation.LESS_THAN)) {
                predicates.add(builder.or(builder.lessThan(criteria.getKey(), criteria.getValue().toString())));
            } else if (criteria.getOperation().equals(SearchOperation.GREATER_THAN_EQUAL)) {
                predicates.add(builder.or(builder.greaterThanOrEqualTo(criteria.getKey(), criteria.getValue().toString())));
            } else if (criteria.getOperation().equals(SearchOperation.LESS_THAN_EQUAL)) {
                predicates.add(builder.or(builder.lessThanOrEqualTo(criteria.getKey(), criteria.getValue().toString())));
            } else if (criteria.getOperation().equals(SearchOperation.NOT_EQUAL)) {
                predicates.add(builder.or(builder.notEqual(criteria.getKey(), criteria.getValue())));
            } else if (criteria.getOperation().equals(SearchOperation.EQUAL)) {
                predicates.add(builder.or(builder.equal(criteria.getKey(), criteria.getValue())));
            } else if (criteria.getOperation().equals(SearchOperation.MATCH)) {
                predicates.add(builder.or(builder.like(builder.lower(criteria.getKey()), "%" + criteria.getValue().toString().toLowerCase() + "%")));
            } else if (criteria.getOperation().equals(SearchOperation.MATCH_INTEGER)) {
                predicates.add(builder.or(builder.like(criteria.getKey(), "%" + criteria.getValue().toString() + "%")));
            } else if (criteria.getOperation().equals(SearchOperation.MATCH_END)) {
                predicates.add(builder.or((builder.like(builder.lower(criteria.getKey()), criteria.getValue().toString().toLowerCase() + "%"))));
            }
        }

        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
