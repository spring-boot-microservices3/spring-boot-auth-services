package springboot.core.model.dto.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserMenuResponse {

    String menuId;
    boolean collapse;
    String state;
    String path;
    String name;
    String icon;
    String component;
    String layout;
    int orderMenu;
    boolean isActive;
    boolean isCreate;
    boolean isRead;
    boolean isUpdate;
    boolean isDelete;
    List<UserMenuResponse> views = new ArrayList<>();
}
