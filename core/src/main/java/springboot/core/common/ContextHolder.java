package springboot.core.common;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class ContextHolder {
    private static final Logger LOG = LoggerFactory.getLogger(ContextHolder.class);

    private ContextHolder() {
    }

    public static UserPrincipal getRequestAuthor() {
        UserPrincipal author = null;
        //if (requestAuthor.get() == null) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getPrincipal() instanceof UserPrincipal) {
            author = (UserPrincipal) auth.getPrincipal();
        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Request author context is undefined");
            }
        }
        // requestAuthor.set(author);
        //}

        //author = requestAuthor.get();
        return author;
    }

    /**
     * Check Login user have permissionCode or not.
     *
     * @param permissionCode
     * @return
     */
    public static boolean hasPermission(String permissionCode) {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .filter(x -> x.getAuthority().equals(permissionCode)).findAny().orElse(null) != null;
    }

    public static String getIpAddress() {
        HttpServletRequest curRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        String ipAddress = "127.0.0.1";
        if (StringUtils.isNotEmpty(curRequest.getHeader("X-REAL-IP"))) {
            // Multiple redirection, set as the first ip
            ipAddress = curRequest.getHeader("X-REAL-IP").trim();
        } else if (StringUtils.isNotEmpty(curRequest.getHeader("X-FORWARDED-FOR"))) {
            // Multiple redirection, set as the first ip
            String[] ips = curRequest.getHeader("X-FORWARDED-FOR").split(",");
            ipAddress = ips[0].trim();
        } else if (StringUtils.isNotEmpty(curRequest.getHeader("HTTP_CLIENT_IP"))) {
            ipAddress = curRequest.getHeader("HTTP_CLIENT_IP");
        } else if (StringUtils.isNotEmpty(curRequest.getHeader("HTTP_X_FORWARDED_FOR"))) {
            // Multiple redirection, set as the first ip
            String[] ips = curRequest.getHeader("HTTP_X_FORWARDED_FOR").split(",");
            ipAddress = ips[0].trim();
        } else if (StringUtils.isNotEmpty(curRequest.getHeader("HTTP_X_FORWARDED")))
            ipAddress = curRequest.getHeader("HTTP_X_FORWARDED");
        else if (StringUtils.isNotEmpty(curRequest.getHeader("HTTP_X_CLUSTER_CLIENT_IP")))
            ipAddress = curRequest.getHeader("HTTP_X_CLUSTER_CLIENT_IP");
        else if (StringUtils.isNotEmpty(curRequest.getHeader("HTTP_FORWARDED_FOR")))
            ipAddress = curRequest.getHeader("HTTP_FORWARDED_FOR");
        else if (StringUtils.isNotEmpty(curRequest.getHeader("HTTP_FORWARDED")))
            ipAddress = curRequest.getHeader("HTTP_FORWARDED");
        else if (StringUtils.isNotEmpty(curRequest.getHeader("REMOTE_ADDR")))
            ipAddress = curRequest.getHeader("REMOTE_ADDR");
        else
            ipAddress = curRequest.getRemoteAddr();

        return ipAddress;
    }
}
