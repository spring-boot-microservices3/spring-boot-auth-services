package springboot.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import springboot.core.config.RepositoryConfig;
import springboot.core.model.dto.RoleDTO;
import springboot.core.model.entity.MasterMenu;
import springboot.core.model.entity.MasterRole;
import springboot.core.repository.MasterMenuRepository;
import springboot.core.services.MenuRoleServices;
import springboot.core.services.RoleServices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created by f.putra on 26/02/21.
 */
@SpringBootApplication(scanBasePackages = {"springboot.core"})
@Import({RepositoryConfig.class})
public class Core {

    public static Logger getLogger(Object o) {
        return LogManager.getLogger(o.getClass());
    }

    @Bean
    public CommandLineRunner loadDataMenu(MasterMenuRepository repository) {
        return (args) -> {
            if (!repository.existsByNameIgnoreCase("Home".toLowerCase()))
                repository.save(new MasterMenu(UUID.randomUUID().toString(), false, null, "/home", "Home", "home", "0", "Home", "/admin", 0));
        };
    }

    @Bean
    public CommandLineRunner loadDataRole(RoleServices roleServices, MenuRoleServices menuRoleServices, MasterMenuRepository masterMenuRepository) {
        return (args) -> {
            if (!roleServices.existsByRoleName("USER", 1)){
                MasterRole masterRole = roleServices.saveRole(new RoleDTO("USER", 1, "Default System"));
                MasterMenu masterMenu = masterMenuRepository.findByName("Home");
                List<String> listRole = new ArrayList<>();
                listRole.add(masterRole.getRoleId());
                menuRoleServices.saveRoleMenu(listRole, Collections.singletonList(masterMenu.getMenuId()));
            }

            if (!roleServices.existsByRoleName("SUPER ADMIN", 2)){
                MasterRole masterRole = roleServices.saveRole(new RoleDTO("SUPER ADMIN", 2, "Default System"));
                MasterMenu masterMenu = masterMenuRepository.findByName("Home");
                List<String> listRole = new ArrayList<>();
                listRole.add(masterRole.getRoleId());
                menuRoleServices.saveRoleMenu(listRole, Collections.singletonList(masterMenu.getMenuId()));
            }
        };
    }
}
