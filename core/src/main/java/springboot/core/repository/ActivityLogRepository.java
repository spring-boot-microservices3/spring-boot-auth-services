package springboot.core.repository;

import springboot.core.model.entity.ActivityLog;

/**
 * Created by f.putra on 1/12/21.
 */
public interface ActivityLogRepository extends BaseRepository<ActivityLog, String> {
}

