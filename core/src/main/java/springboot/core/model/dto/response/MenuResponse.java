package springboot.core.model.dto.response;

import lombok.Data;
@Data
public class MenuResponse {
     private String menuId;
     private boolean collapse = false;
     private String state;
     private String path;
     private String name;
     private String icon;
     private String parent;
     private String parentName;
     private String component;
     private String layout;
     int orderMenu;
     boolean isDelete;
}
