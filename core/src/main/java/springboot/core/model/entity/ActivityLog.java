package springboot.core.model.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by f.putra on 1/12/21.
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "LOG_ACTIVITY")
@TypeDef(
        name = "jsonb",
        typeClass = JsonBinaryType.class
)
public class ActivityLog extends Auditable<String> implements Serializable {

    private static final long serialVersionUID = 4577272204006973181L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    private String id;
    @Column(name = "TABLE_NAME")
    private String TABLE_NAME;
    @Column(name = "ACTION")
    private String ACTION;
    @Column(name = "ROW_ID")
    private String ROW_ID;

    @Type(type = "jsonb")
    @Column(name = "OLD_DATA", columnDefinition = "jsonb")
    private String OLD_DATA;

    @Type(type = "jsonb")
    @Column(name = "NEW_DATA", columnDefinition = "jsonb")
    private String NEW_DATA;
}
