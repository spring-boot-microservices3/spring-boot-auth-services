package springboot.core.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_menu", uniqueConstraints ={
        @UniqueConstraint(columnNames = {"name", "path", "order_menu"})
})
public class MasterMenu extends Auditable<String> implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "menu_id")
    private String menuId;

    @Column(name = "collapse", nullable = false)
    private boolean collapse = false;

    @Column(name = "state")
    private String state;

    @Column(name = "path")
    private String path;

    @Column(name = "name")
    private String name;

    @Column(name = "icon")
    private String icon;

    @Column(name = "parent")
    private String parent;

    @Column(name = "component")
    private String component;

    @Column(name = "layout")
    private String layout;

    @Column(name = "order_menu")
    private int orderMenu;
}
