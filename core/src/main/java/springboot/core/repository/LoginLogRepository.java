package springboot.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import springboot.core.model.entity.LoginLog;

import java.util.Optional;

/**
 * Created by f.putra on 1/12/21.
 */
public interface LoginLogRepository extends BaseRepository<LoginLog, String> {

    @Query("SELECT ll FROM LoginLog ll WHERE ll.USERNAME = :username AND ll.TIME_LOGOUT IS NULL")
    Optional<LoginLog> findUser(@Param("username") String username);
}
