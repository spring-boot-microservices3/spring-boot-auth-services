package springboot.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import springboot.core.model.entity.MenuRole;

import java.util.List;

public interface MenuRoleRepository extends BaseRepository<MenuRole, String> {

    @Transactional(readOnly = true)
    @Query("SELECT mr FROM MenuRole mr WHERE mr.roleId = :roleId")
    List<MenuRole> findMenuByRole(@Param("roleId") String roleId);

    @Transactional(readOnly = true)
    @Query("SELECT mr FROM MenuRole mr WHERE mr.menuId = :menu_id AND mr.roleId = :role_id")
    MenuRole findByMenuIdAndRoleId(@Param("menu_id") String menuId, @Param("role_id") String roleId);
}
