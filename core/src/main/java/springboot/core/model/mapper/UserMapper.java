package springboot.core.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import springboot.core.model.dto.response.UserResponse;
import springboot.core.model.entity.MasterUser;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mappings({
            @Mapping(target = "role.name", source = "masterRole.roleName"),
            @Mapping(target = "role.roleId", source = "masterRole.roleId"),
            @Mapping(target = "role.level", source = "masterRole.level")
    })
    UserResponse toUserResponse(MasterUser user);
}
