package springboot.rest.security.helper;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import springboot.core.Core;
import springboot.core.common.TimeProvider;
import springboot.core.common.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by f.putra on 15/02/18.
 */
@Component
public class TokenHelper {

    @Value("${security.security-realm}")
    private String APP_NAME;

    @Value("${security.jwt.client-secret}")
    public String SECRET;

    @Value("${security.jwt.expiration}")
    private int EXPIRES_IN;

    @Value("${security.jwt.mobile_expires_in}")
    private int MOBILE_EXPIRES_IN;

    @Value("${security.jwt.header}")
    private String AUTH_HEADER;

    @Autowired
    TimeProvider timeProvider;

    static final String AUDIENCE_UNKNOWN = "unknown";
    static final String AUDIENCE_WEB = "web";
    static final String AUDIENCE_MOBILE = "mobile";
    static final String AUDIENCE_TABLET = "tablet";

//    TimeProvider timeProvider;

    private final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;

    public String getUsernameFromToken(String token) {
        String username;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    public Date getIssuedAtDateFromToken(String token) {
        Date issueAt;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            issueAt = claims.getIssuedAt();
        } catch (Exception e) {
            issueAt = null;
        }
        return issueAt;
    }

        public String generateToken(String username, Date loginAt) {
        return Jwts.builder()
                .setIssuer( APP_NAME )
                .setSubject(username)
                .setIssuedAt(loginAt)
                .signWith( SIGNATURE_ALGORITHM, SECRET )
                .setExpiration(generateExpirationDate())
                .compact();
    }

    private Claims getAllClaimsFromToken(String token) {
        try {
            return Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            return null;
        }
    }

    private Date generateExpirationDate() {
//        long expiresIn = device.isTablet() || device.isMobile() ? MOBILE_EXPIRES_IN : EXPIRES_IN;
        return new Date(timeProvider.now().getTime() + EXPIRES_IN );
    }

    public Boolean validateToken(String token, UserPrincipal userDetails) {
        try {
            Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token);

            // validate last session for only 1 session
//            String lastLogin = DateUtils.formatDateTime(getIssuedAtDateFromToken(token), DateUtils.YYYY_MM_DD_HH_MM_SS);
//            String userLogin = DateUtils.formatDateTime(userDetails.getLastLoginAt(), DateUtils.YYYY_MM_DD_HH_MM_SS);
//            return userLogin.equals(lastLogin);
            return true;
        } catch (SignatureException ex) {
            Core.getLogger(this).error("Invalid JWT signature " + userDetails.getUsername());
        } catch (MalformedJwtException ex) {
            Core.getLogger(this).error("Invalid JWT token " + userDetails.getUsername());
        } catch (ExpiredJwtException ex) {
            Core.getLogger(this).error("Expired JWT token "  + userDetails.getUsername());
        } catch (UnsupportedJwtException ex) {
            Core.getLogger(this).error("Unsupported JWT token "  + userDetails.getUsername());
        } catch (IllegalArgumentException ex) {
            Core.getLogger(this).error("JWT claims string is empty. "  + userDetails.getUsername());
        }
        return false;
    }


    public String getToken( HttpServletRequest request ) {
        /**
         *  Getting the token from Authentication header
         *  e.g Bearer your_token
         */
        String authHeader = getAuthHeaderFromHeader( request );
        if ( authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7);
        }
        return null;
    }

    public String getAuthHeaderFromHeader( HttpServletRequest request ) {
        return request.getHeader(AUTH_HEADER);
    }

}
