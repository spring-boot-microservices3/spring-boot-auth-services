package springboot.rest.security.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class SupportingAuthParameter {

    Map<String, String> params = new HashMap<String, String>();

    @JsonProperty("email")
    public String getEmail() {
        return params.get("email");
    }

    public void setEmail(String email) {
        params.put("email", email);
    }

    @JsonProperty("username")
    public String getUsername() {
        return params.get("username");
    }

    public void setUsername(String username) {
        params.put("username", username);
    }

    @JsonProperty("password")
    public String getPassword() {
        return params.get("password");
    }

    public void setPassword(String password) {
        params.put("password", password);
    }

    public Map<String, String> buildMap() {
        return params;
    }
}
