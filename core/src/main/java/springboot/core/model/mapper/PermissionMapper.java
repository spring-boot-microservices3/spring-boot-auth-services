package springboot.core.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import springboot.core.model.dto.response.UserMenuResponse;
import springboot.core.model.entity.MenuRole;

import java.util.List;

@Mapper
public interface PermissionMapper {
    PermissionMapper INSTANCE = Mappers.getMapper(PermissionMapper.class);

    @Mappings({
            @Mapping(target = "menuId", source = "menuId"),
            @Mapping(target = "collapse", source = "menu.collapse"),
            @Mapping(target = "state", source = "menu.state"),
            @Mapping(target = "path", source = "menu.path"),
            @Mapping(target = "name", source = "menu.name"),
            @Mapping(target = "icon", source = "menu.icon"),
            @Mapping(target = "component", source = "menu.component"),
            @Mapping(target = "layout", source = "menu.layout"),
            @Mapping(target = "orderMenu", source = "menu.orderMenu"),
            @Mapping(target = "active", source = "active"),
            @Mapping(target = "create", source = "create"),
            @Mapping(target = "read", source = "read"),
            @Mapping(target = "update", source = "update"),
            @Mapping(target = "delete", source = "delete"),
    })
    UserMenuResponse toResponse(MenuRole menuRole);

    List<UserMenuResponse> toResponseList(List<MenuRole> menuRole);
}
