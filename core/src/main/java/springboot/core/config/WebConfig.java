package springboot.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import springboot.core.common.ResourceBundle;

@Configuration
public class WebConfig {

  @Bean
  ResourceBundle resourceBundle() {
    return new ResourceBundle();
  }

  @Bean
  ReloadableResourceBundleMessageSource messageSource() {
    ReloadableResourceBundleMessageSource resourceBundleMessageSource = new ReloadableResourceBundleMessageSource();
    resourceBundleMessageSource.setBasename("classpath:messages/messages");
    resourceBundleMessageSource.setDefaultEncoding("UTF-8");
    return resourceBundleMessageSource;
  }

}
