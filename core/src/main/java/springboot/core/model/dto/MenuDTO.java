package springboot.core.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MenuDTO extends BaseDTO implements Serializable {

    private static final long serialVersionUID = -7940437445253019789L;

    private Boolean collapse;
    private String state;
    private String path;
    private String name;
    private String icon;
    private String parent = "0";
    private String component;
    private String layout;
    private Integer order;
}
