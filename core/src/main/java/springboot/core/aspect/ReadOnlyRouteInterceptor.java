package springboot.core.aspect;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import springboot.core.Core;
import springboot.core.common.ContextHolder;
import springboot.core.common.StringUtils;
import springboot.core.config.RoutingDataSource;
import springboot.core.model.entity.ActivityLog;
import springboot.core.services.LogServices;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import springboot.core.common.ResourceBundle;
import springboot.core.services.LogServices;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/**
 * Created by f.putra on 3/12/20.
 */
@Aspect
@Component
@Order(0)
public class ReadOnlyRouteInterceptor {

    @Autowired
    LogServices logServices;
    @Autowired
    ResourceBundle resourceBundle;

    @Around("@annotation(transactional) && !execution(* delete*(..))")
    public Object proceed(ProceedingJoinPoint proceedingJoinPoint, Transactional transactional) throws Throwable {
        try {
            if (transactional.readOnly()) {
                RoutingDataSource.setReplicaRoute();
//                logger.info("Routing database call to the read replica");
            } else {
//                Core.getLogger(this).info(transactional.value());
            }
            return proceedingJoinPoint.proceed();
        } catch (DuplicateKeyException e) {

//            ErrorLog errorLog = new ErrorLog();
//            errorLog.setERROR_SOURCE(e.getMessage());
//            errorLog.setERROR_MESSAGE("DUPLICATE KEY EXCEPTION");
//            errorLog.setERROR_TIME(new Date());
//            errorLog.setUSERNAME(ContextHolder.getRequestAuthor().getUsername());
//            logServices.saveErrorLog(errorLog);

            throw new DuplicateKeyException(resourceBundle.getMessage("err.data.duplicate"));
        } catch (DataIntegrityViolationException e) {

//            ErrorLog errorLog = new ErrorLog();
//            errorLog.setERROR_SOURCE(e.getMessage());
//            errorLog.setERROR_MESSAGE("DATA EXIST");
//            errorLog.setERROR_TIME(new Date());
//            errorLog.setUSERNAME(ContextHolder.getRequestAuthor().getUsername());
//            logServices.saveErrorLog(errorLog);

            throw new DataIntegrityViolationException(resourceBundle.getMessage("err.data.existed"));
        } catch (IllegalArgumentException e) {
//            ErrorLog errorLog = new ErrorLog();
//            errorLog.setERROR_SOURCE(e.getMessage());
//            errorLog.setERROR_MESSAGE("ILLEGAL ARGUMENT EXCEPTION");
//            errorLog.setERROR_TIME(new Date());
//            errorLog.setUSERNAME(ContextHolder.getRequestAuthor().getUsername());
//            logServices.saveErrorLog(errorLog);

            throw e;
        } catch (DataAccessException e) {
//            ErrorLog errorLog = new ErrorLog();
//            errorLog.setERROR_SOURCE(e.getMessage());
//            errorLog.setERROR_MESSAGE("DATA ACCESS PROBLEM");
//            errorLog.setERROR_TIME(new Date());
//            errorLog.setUSERNAME(ContextHolder.getRequestAuthor().getUsername());
//            logServices.saveErrorLog(errorLog);

            throw e;
        } finally {
            RoutingDataSource.clearReplicaRoute();
        }
    }

    @AfterReturning(pointcut = "execution(* springboot.core.services.*.save*(..))", returning = "result")
    public void logAfterReturning(JoinPoint joinPoint, Object result) throws JsonProcessingException {
        String method = StringUtils.join(
                StringUtils.splitByCharacterTypeCamelCase(joinPoint.getSignature().getName()),
                ' '
        );
        String action = Arrays.stream(StringUtils.splitByCharacterTypeCamelCase(joinPoint.getSignature().getName())).findFirst().get().toUpperCase(Locale.ROOT);
        if (result != null) {
            ActivityLog activityLog = new ActivityLog();
            activityLog.setTABLE_NAME(method);
            activityLog.setROW_ID("");
            activityLog.setACTION(action);
            activityLog.setOLD_DATA(null);
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(result);
            activityLog.setNEW_DATA(json);
            activityLog.setCreatedBy(ContextHolder.getRequestAuthor() != null ? ContextHolder.getRequestAuthor().getUsername() : "SYSTEM");
            activityLog.setCreatedDate(new Date());
            logServices.saveActivityLog(activityLog);
        } else {
            Core.getLogger(this).info(" ###### with null as return value.");
        }
    }
}
