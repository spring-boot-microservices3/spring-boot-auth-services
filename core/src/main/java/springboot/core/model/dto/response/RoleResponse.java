package springboot.core.model.dto.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RoleResponse {

    String roleId;
    String name;
    Integer level;
    List<UserMenuResponse> menus = new ArrayList<>();
}
