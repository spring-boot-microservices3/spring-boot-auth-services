package springboot.core.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import springboot.core.Core;
import springboot.core.common.ResourceBundle;
import springboot.core.common.*;
import springboot.core.model.dto.ActivityGenericLog;
import springboot.core.model.dto.UserDTO;
import springboot.core.model.dto.response.UserMenuResponse;
import springboot.core.model.dto.response.UserResponse;
import springboot.core.model.entity.*;
import springboot.core.model.mapper.PermissionMapper;
import springboot.core.model.mapper.UserMapper;
import springboot.core.repository.*;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;

/**
 * Created by f.putra on 9/8/20.
 */
@Service
public class UserServices extends BaseCriteriaBuilder implements UserDetailsService {
    @Autowired
    MasterUserRepository userRepository;

    @Autowired
    MasterRoleRepository roleRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    MasterMenuRepository masterMenuRepository;

    @Autowired
    MenuRoleRepository menuRoleRepository;

    @Autowired
    MenuRoleServices menuRoleServices;
    @Autowired
    LogServices logServices;
    @Autowired
    ResourceBundle resourceBundle;

    @Transactional
    public UserResponse registerUser(UserDTO userDTO) throws JsonProcessingException {
        String email = userDTO.getEmail();
        String username = userDTO.getUsername();

        if (userRepository.existsByUsernameOrEmail(username, email))
            throw new IllegalArgumentException(resourceBundle.getMessage("err.user.validation.data_existed"));

        MasterUser user = new MasterUser();
        user.setUsername(userDTO.getUsername());
        user.setEmail(email);
        user.setPassword(StringUtils.encryptPassword(userDTO.getPassword()));
        user.setActive(userDTO.getIsDelete());

        user = userRepository.save(user);
        UserRole userRole = new UserRole();
        userRole.setUserId(user.getUserId());

        if (userDTO.getRole() != null && !userDTO.getRole().isEmpty()) {
            MasterRole role = roleRepository.findById(userDTO.getRole()).get();
            userRole.setRoleId(role.getRoleId());
            userRole.setMasterRole(role);
        } else {
            MasterRole role = roleRepository.findByRoleName("USER");
            userRole.setRoleId(role.getRoleId());
            userRole.setMasterRole(role);
        }

        userRoleRepository.save(userRole);
        logServices.saveGeneralActivityLog(new ActivityGenericLog(null, UserMapper.INSTANCE.toUserResponse(user), "Register User"));
        return UserMapper.INSTANCE.toUserResponse(user);
    }

    @Transactional
    public UserResponse updateUser(String id, UserDTO dto) throws JsonProcessingException {
        Optional<MasterUser> optional = userRepository.findById(id);
        if (!optional.isPresent()) {
            throw new UsernameNotFoundException("User not found.");
        }
        logServices.saveGeneralActivityLog(new ActivityGenericLog(optional.get(), dto, "Master User"));
        MasterUser user = optional.get();
        user.setUsername(dto.getUsername());
        user.setEmail(dto.getEmail());
        user.setActive(dto.getIsDelete() != null ? dto.getIsDelete() : true);

        try {
            user = userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new IllegalArgumentException(resourceBundle.getMessage("err.user.validation.data_existed"));
        }

        UserRole userRole = userRoleRepository.findByUserId(user.getUserId());
        if (userRole == null) userRole = new UserRole();

        MasterRole role;
        if (dto.getRole() != null && !dto.getRole().isEmpty()) {
            role = roleRepository.findById(dto.getRole()).get();
        } else {
            role = roleRepository.findByRoleName("USER");
        }

        userRole.setUserId(user.getUserId());
        userRole.setRoleId(role.getRoleId());
        userRole.setMasterRole(role);

        try {
            userRoleRepository.save(userRole);
            return UserMapper.INSTANCE.toUserResponse(user);
        } catch (DataIntegrityViolationException e) {
            throw e;
        }
    }

    /**
     * load user for token JWT
     *
     * @param username
     * @return
     */
    @Transactional(readOnly = true)
    public UserPrincipal loadUserByUsername(String username) {
        MasterUser user = userRepository.getByUsername(username);
        if (null == user) {
            throw new UsernameNotFoundException("User " + username + " not found.");
        }

        return new UserPrincipal(user);
    }

    /**
     * get user By user id
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    public MasterUser getUserById(String id) {
        try {
            return userRepository.getUser(id);
        } catch (NoResultException e) {
            throw e;
        } catch (Exception e) {
            Core.getLogger(this).error(e.getMessage());
            throw e;
        }
    }

    /**
     * get all user
     *
     * @return
     * @throws Exception
     */
    @Transactional(readOnly = true)
    public Page<MasterUser> getAllUser(Pageable pageable, UserDTO userDTO) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<MasterUser> cq = cb.createQuery(MasterUser.class);

        Root<MasterUser> from = cq.from(MasterUser.class);
        Join<MasterUser, MasterRole> role = from.join("masterRole", JoinType.LEFT);
        applyOrderByCreatedAndUpdateDate(cb, from, cq);

        List<Predicate> predicates = new ArrayList<>();
        GenericSpesification genericSpesification = new GenericSpesification<MasterUser>();
        if (!StringUtils.isBlank(userDTO.getFullname())) {
            predicates.add(cb.like(cb.lower(from.get("EMPLOYEE_FULLNAME")), "%".concat(userDTO.getFullname().toLowerCase().concat("%"))));
        }
        if (!StringUtils.isBlank(userDTO.getRole())) {
            predicates.add(cb.like(cb.upper(role.get("roleName")), userDTO.getRole().toUpperCase()));
        }
        if (userDTO.getIsDelete() != null) {
            predicates.add(cb.equal(from.get("isDelete"), userDTO.getIsDelete()));
        }

        cq.where(predicates.toArray(new Predicate[0]));

        TypedQuery<MasterUser> query = entityManager.createQuery(cq)
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize());

        long totalRows = userRepository.count(genericSpesification);

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public void logout(String userId) {
        MasterUser user = this.getUserById(userId);
        user.setLastLoginAt(null); // reset last login
        try {
            logServices.updateLoginLog(new LoginLog(user.getUserId(), user.getUsername(), null, null, null, null, null, "LOGIN"));
            userRepository.save(user);
        } catch (Exception e) {
            throw e;
        }
    }

    public UserResponse deleteUser(String id) {
        Optional<MasterUser> optional = userRepository.findById(id);
        if (!optional.isPresent()) {
            throw new UsernameNotFoundException("User not found.");
        }

        MasterUser user = optional.get();
        user.setActive(false);
        return UserMapper.INSTANCE.toUserResponse(userRepository.save(user));
    }

    public UserResponse getMenuRoleAfterLogin(String userId) {
        UserResponse userResponse = UserMapper.INSTANCE.toUserResponse(getUserById(userId));
        boolean roleActive = roleActive(userResponse.getRole().getRoleId());
        if (!roleActive)
            throw new RestClientException(resourceBundle.getMessage("err.role.validation.inactive", userResponse.getRole().getName()));

        List<MenuRole> menuRoles = menuRoleServices.getAllByRole(userResponse.getRole().getRoleId());
        List<UserMenuResponse> menus = new ArrayList<>();
        getRoot(menuRoles, menus);
        menus.parallelStream().forEach(menu -> {
            createMenuTree(menuRoles, menu);
            menu.getViews().sort(Comparator.comparingInt(UserMenuResponse::getOrderMenu));
        });
        userResponse.getRole().setMenus(menus);
        return userResponse;
    }

    public UserResponse getUserProfile() {
        return getMenuRoleAfterLogin(ContextHolder.getRequestAuthor().getId());
    }

    private void getRoot(List<MenuRole> menus, List<UserMenuResponse> userMenuResponses) {
        for (MenuRole iterator : menus) {
            MasterMenu menu = iterator.getMenu();
            if (menu.getParent().equals("0")) {
                // After obtaining the root node information, delete the root node information from the list
//                menus.remove(iterator);
                userMenuResponses.add(PermissionMapper.INSTANCE.toResponse(iterator));
            }
        }
        //sorting parent menu
        userMenuResponses.sort(Comparator.comparingInt(UserMenuResponse::getOrderMenu));
    }

    private void createMenuTree(List<MenuRole> menus, UserMenuResponse parentNode) {
        //This is judged here to prevent access to an empty root node and cause access to the root node information.
        // Generate an exception

        if (null != parentNode) {
            menus.parallelStream().forEach(value -> {
                if (parentNode.getMenuId().equals(value.getMenu().getParent())) {
                    UserMenuResponse node = PermissionMapper.INSTANCE.toResponse(value);
                    parentNode.getViews().add(node);
                    createMenuTree(menus, node);
                }
            });
        }
    }

    public boolean roleActive(String roleId) {
        Optional<MasterRole> optional = roleRepository.findById(roleId);
        /**
         * TDOO : fixing role active or not
         */
        return true;
//        return optional.map(role -> !role.isDelete()).orElse(true);
    }

    public void saveLastLogin(String id, Date loginAt) {
        MasterUser user = getUserById(id);
        if (user.getLastLoginAt() != null) {
            throw new RestClientException(resourceBundle.getMessage("err.user.validation.logged"));
        }
        user.setLastLoginAt(loginAt);
        userRepository.save(user);
    }

    public List<MasterUser> checkUserLogin() {
        return userRepository.findByLastLoginAtIsNotNull();
    }

    @Transactional
    public MasterUser saveUser(MasterUser user) {
        try {
            return userRepository.save(user);
        } catch (Exception e) {
            throw e;
        }
    }
}
