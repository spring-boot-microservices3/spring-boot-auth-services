package springboot.rest.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import springboot.core.common.ContextHolder;
import springboot.core.common.ResourceBundle;
import springboot.core.common.UserPrincipal;
import springboot.rest.exception.CustomExceptionDTO;
import springboot.rest.exception.RestClientException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;


public abstract class AbstractRestController {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRestController.class);

    @Autowired
    protected ResourceBundle resourceBundle;

    protected UserPrincipal getCurrentUser() {
        return ContextHolder.getRequestAuthor();
    }

    /**
     * Throws failed response
     *
     * @param status
     * @param message
     * @return
     */
    protected ResponseEntity<RestResult<String>> createErrorResponse(HttpStatus status, String message) {
        RestResult<String> restResult = new RestResult<>();
        restResult.fail(message);
        return new ResponseEntity<>(restResult, status);
    }

    // ============== HANDLE EXCEPTION ===========//

    /**
     * @param exception: authentication request is rejected
     * @param request:   http request
     * @return
     */
    @ExceptionHandler({AuthenticationCredentialsNotFoundException.class})
    @ResponseBody
    public ResponseEntity<RestResult<Object>> handleUnauthorizedException(AuthenticationCredentialsNotFoundException exception,
                                                                          HttpServletRequest request) {
        logException(exception, request);
        String message = resourceBundle.getMessage("err.auth.client_credentials_not_found");
        return createExceptionResponse(HttpStatus.UNAUTHORIZED, Arrays.asList(message),
                new CustomExceptionDTO(message, "err.auth.client_credentials_not_found", getTraceId()));
    }

    @ExceptionHandler({AccessDeniedException.class})
    @ResponseBody
    public ResponseEntity<RestResult<Object>> handleAccessDeniedException(AccessDeniedException exception, HttpServletRequest request) {
        logException(exception, request);
        String message = resourceBundle.getMessage("err.auth.access_denied");
        List<String> errors = getRecMsgErrors(exception, Arrays.asList(message));
        return createExceptionResponse(HttpStatus.FORBIDDEN, errors,
                new CustomExceptionDTO(message, "err.auth.access_denied", getTraceId()));
    }

    /**
     * Handler wrapper exception throwed by Micro-service
     *
     * @param exception
     * @param request
     * @return
     */
    @ExceptionHandler({RestClientException.class})
    @ResponseBody
    public ResponseEntity<RestResult<Object>> handleRestClientException(RestClientException exception, HttpServletRequest request) {
        logException(exception, request);
        List<String> errors = getRecMsgErrors(exception, null);
        return createExceptionResponse(exception.getHttpStatusCode(), errors,
                new CustomExceptionDTO(exception.getMessage(), exception.getErrorCode(), getTraceId()));
    }

    @ExceptionHandler({NoSuchElementException.class})
    @ResponseBody
    public ResponseEntity<RestResult<Object>> handleNotFoundException(NoSuchElementException exception, HttpServletRequest request) {
        logException(exception, request);
        List<String> exceptions = getRecMsgErrors(exception, null);
        return createExceptionResponse(HttpStatus.BAD_REQUEST, exceptions,
                new CustomExceptionDTO(exception.getMessage(), "err.data.not_found", getTraceId()));
    }

    @ExceptionHandler({IllegalArgumentException.class, IllegalStateException.class})
    @ResponseBody
    public ResponseEntity<RestResult<Object>> handleIllegalException(Exception exception, HttpServletRequest request) {
        logException(exception, request);
        List<String> exceptions = getRecMsgErrors(exception, null);
        return createExceptionResponse(HttpStatus.BAD_REQUEST, exceptions,
                new CustomExceptionDTO(exception.getMessage(), "err.client.illegal_argument", getTraceId()));
    }

    /**
     * Wrong INPUT data : missing required data, bad format data
     *
     * @param exception
     * @param request
     * @return
     */
    @ExceptionHandler({ServletException.class, HttpMessageNotReadableException.class, MethodArgumentNotValidException.class})
    @ResponseBody
    public ResponseEntity<RestResult<Object>> handleClientException(Exception exception, HttpServletRequest request) {
        logException(exception, request);
        List<String> exceptions = getRecMsgErrors(exception, null);
        return createExceptionResponse(HttpStatus.BAD_REQUEST, exceptions,
                new CustomExceptionDTO(exception.getMessage(), "err.client.illegal_argument", getTraceId()));
    }

    /**
     * Unexpected runtime Exception
     *
     * @param exception
     * @param request
     * @return
     */
    @ExceptionHandler({RuntimeException.class})
    @ResponseBody
    public ResponseEntity<RestResult<Object>> handleUnexpectedException(RuntimeException exception, HttpServletRequest request) {
        logException(exception, request);
        String generalMsg = resourceBundle.getMessage("error.server.unexpected");
        List<String> exceptions = getRecMsgErrors(exception, null);
        exceptions.add(0, generalMsg);
        return createExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR, exceptions,
                new CustomExceptionDTO(generalMsg, "error.server.unexpected", getTraceId()));
    }

    // === Final catch ===//
    @ExceptionHandler({Exception.class})
    @ResponseBody
    public ResponseEntity<RestResult<Object>> handleException(Exception exception, HttpServletRequest request) {
        logException(exception, request);
        String generalMsg = resourceBundle.getMessage("error.server.unexpected");
        List<String> exceptions = getRecMsgErrors(exception, null);
        exceptions.add(0, generalMsg);
        return createExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR, exceptions,
                new CustomExceptionDTO(generalMsg, "error.server.unexpected", getTraceId()));
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<RestResult<Object>> handleMaxSizeException(MaxUploadSizeExceededException exception, HttpServletRequest request) {
        logException(exception, request);
        String generalMsg = resourceBundle.getMessage("err.file.too_large");
        List<String> exceptions = getRecMsgErrors(exception, null);
        exceptions.add(0, generalMsg);
        return createExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR, exceptions,
                new CustomExceptionDTO(generalMsg, "err.file.too_large", getTraceId()));
    }

    // ============== PRIVATE FUNCS ===========//

    /**
     * log exception to server
     *
     * @param e
     * @param request
     */
    protected void logException(Exception e, HttpServletRequest request) {
        UserPrincipal user = getCurrentUser();
        // maybe log context is not set yet
        String actor = (user != null ? user.getUsername() : "Anonymous");
        getLogger().error("{}@{} - {} : {}", actor, request.getHeader("User-Agent"), request.getRequestURI(), e.getMessage(), e);

    }

    protected Logger getLogger() {
        return LOG;
    }

    /**
     * create response for error
     *
     * @param httpStatus
     * @param messages
     * @param data
     * @return
     */
    protected ResponseEntity<RestResult<Object>> createExceptionResponse(HttpStatus httpStatus, List<String> messages,
                                                                         CustomExceptionDTO data) {
        RestResult<Object> result = new RestResult<>();
        result.setStatus(RestResult.STATUS_ERROR).setData(data);
        for (String message : messages) {
            result.addMessage(message);
        }
        return new ResponseEntity<>(result, httpStatus);
    }

    protected List<String> getRecMsgErrors(Throwable e, List<String> msgs) {
        //Cover unmodifiable list of msgs
        ArrayList<String> errors = new ArrayList<>();
        if (msgs != null) {
            errors.addAll(msgs);
        }

        errors.add(e.getMessage());
        if (e.getCause() != null) {
            return getRecMsgErrors(e.getCause(), errors);
        }

        return errors;
    }

    protected String getTraceId() {
        // FIXME
        return "0001";
    }

}
