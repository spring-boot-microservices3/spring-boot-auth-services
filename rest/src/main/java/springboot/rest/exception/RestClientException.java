package springboot.rest.exception;

import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Transfering micro-service exception
 * Keep error messages and http status code that is return by remote micro-services
 */
public class RestClientException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -594950573892941715L;

    private final HttpStatus httpStatusCode;
    private final List<String> errors;
    private final String errorCode;

    public RestClientException(HttpStatus httpStatusCode, String... messages) {
        this(httpStatusCode, Arrays.asList(messages));
    }

    public RestClientException(HttpStatus httpStatusCode, List<String> messages) {
        super(messages.isEmpty() ? "" : messages.get(0));
        this.httpStatusCode = httpStatusCode;
        this.errors = messages;
        this.errorCode = "err";
    }

    public RestClientException(HttpStatus httpStatusCode, List<String> messages, String errorCode) {
        super(messages.isEmpty() ? "" : messages.get(0));
        this.httpStatusCode = httpStatusCode;
        this.errors = messages;
        this.errorCode = errorCode;
    }

    public HttpStatus getHttpStatusCode() {
        return httpStatusCode;
    }

    public List<String> getErrors() {
        return errors;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
