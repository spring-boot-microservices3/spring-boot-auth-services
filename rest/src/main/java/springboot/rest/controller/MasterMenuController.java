package springboot.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springboot.core.model.dto.MenuDTO;
import springboot.core.services.MenuServices;

@RestController
@RequestMapping("menu")
public class MasterMenuController extends BaseController {

    @Autowired
    MenuServices menuServices;

    @PostMapping("/create")
    public ResponseEntity<?> createMenu(@RequestBody MenuDTO payloadMenu) {
        try{
        return getCallBack(menuServices.saveMenu(payloadMenu), HttpStatus.OK);
        }catch (Exception e){
            return getCallBack(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateMenu(@RequestBody MenuDTO payloadMenu) {
        try{
        return getCallBack(menuServices.updateMenu(payloadMenu), HttpStatus.OK);
        }catch (Exception e){
            return getCallBack(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/list")
    public ResponseEntity<?> getMenuList(@RequestParam(name = "offset") Integer offset,
                                         @RequestParam(name = "limit") Integer limit,
                                         @RequestParam(name = "name", required = false) String name,
                                         @RequestParam(name = "path", required = false) String path,
                                         @RequestParam(name = "order", required = false) Integer order,
                                         @RequestParam(name = "parent", required = false) String parent,
                                         @RequestParam(name = "isDelete", required = false) Boolean isDelete,
                                         @RequestParam(name = "collapse", required = false) Boolean collapse) {
        MenuDTO dto = new MenuDTO();
        dto.setName(name);
        dto.setOrder(order);
        dto.setPath(path);
        dto.setParent(parent);
        dto.setIsDelete(isDelete);
        dto.setCollapse(collapse);
        return getCallBack(menuServices.findAllMenu(PageRequest.of(offset, limit), dto), HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<?> getMenuList(@RequestParam(name = "name") String name) {
        return getCallBack(menuServices.searchMenu(name), HttpStatus.OK);
    }

//    @GetMapping("/findParent")
//    public ResponseEntity<?>getMenuList(@RequestParam(name = "parent") String name) {
//        return getCallBack(menuServices.searchMenu(name), HttpStatus.OK);
//    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteMenu(@PathVariable(name = "id") String id) {
        return getCallBack(menuServices.deleteMenu(id), HttpStatus.OK);
    }
}
