package springboot.core.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import springboot.core.model.dto.RoleAccessDTO;
import springboot.core.model.entity.MenuRole;

import java.util.List;

@Mapper
public interface MenuRoleMapper {
    MenuRoleMapper INSTANCE = Mappers.getMapper(MenuRoleMapper.class);

    @Mappings({
            @Mapping(target = "menu_role_id", source = "menuRoleId"),
            @Mapping(target = "menu_id", source = "menuId"),
            @Mapping(target = "role_id", source = "roleId"),
            @Mapping(target = "name", source = "menu.name"),
            @Mapping(target = "icon", source = "menu.icon"),
            @Mapping(target = "path", source = "menu.path"),
            @Mapping(target = "role_name", source = "role.roleName"),
            @Mapping(target = "parent", source = "menu.parent"),
            @Mapping(target = "level", source = "role.level"),
            @Mapping(target = "is_create", source = "create"),
            @Mapping(target = "is_read", source = "read"),
            @Mapping(target = "is_update", source = "update"),
            @Mapping(target = "is_delete", source = "delete"),
            @Mapping(target = "is_active", source = "active"),

    })
    RoleAccessDTO toDTO(MenuRole role);

    List<RoleAccessDTO> toDTOList(List<MenuRole> roles);
}
