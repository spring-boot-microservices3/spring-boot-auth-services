package springboot.core.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import springboot.core.model.dto.response.RoleResponse;
import springboot.core.model.entity.MasterRole;

@Mapper
public interface RoleMapper {
    RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);

    RoleResponse toRoleResponse(MasterRole role);
}
