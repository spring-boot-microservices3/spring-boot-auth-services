package springboot.core.common;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by f.putra on 23/11/19.
 */
@Component
public class TimeProvider implements Serializable {

    private static final long serialVersionUID = 241506593046308436L;

    public Date now() {
        return new Date();
    }
}
