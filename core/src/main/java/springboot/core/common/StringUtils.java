package springboot.core.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringUtils extends org.apache.commons.lang3.StringUtils {

    public static final Pattern PATTERN_NUMBER = Pattern.compile("^\\d+$");

    public static final String INDO_PHONE_NUMBER_FORMAT = "+62%s";
    public static final String INDO_HOME_PHONE_NUMBER_FORMAT = "+62%s";

    /* FORMAT */
    public static final String INDO_PHONE_CODE = "62";
    public static final String INDO_HOME_PHONE_CODE = "62";
    public static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String encryptPassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    private static final Logger log = LogManager.getLogger(StringUtils.class);

    /**
     * create md5 string
     */
    public static String md5(String text) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(text.getBytes());
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte aByteData : byteData) {
                sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    /**
     * encode string base64
     */
    public static String base64Encode(String str) {
        Base64.Encoder encoder = Base64.getMimeEncoder();
        byte[] encoded = encoder.encode(str.getBytes());
        return new String(encoded);
    }

    /**
     * base 64 decode
     */
    public static String base64Decode(String str) {
        Base64.Decoder decoder = Base64.getMimeDecoder();
        byte[] decoded = decoder.decode(str.getBytes());
        return new String(decoded, StandardCharsets.UTF_8);
    }

    /**
     * get full stacktrace of exception
     */
    public static String getStackTrace(Throwable aThrowable) {
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }

    /**
     * validate email
     */
    public static boolean validateEmail(String email) {
        String regexStr = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(regexStr);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * validate uuid
     */
    public static boolean validateUUID(String uuidString) {
        if (uuidString == null) return false;
        try {
            UUID fromStringUUID = UUID.fromString(uuidString);
            String toStringUUID = fromStringUUID.toString();
            return toStringUUID.equals(uuidString);
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    /**
     * get tail of string
     * if required tail length is bigger than string length
     * insert number of char c
     */
    public static String getTail(String s, int numberOfChar, char c) {
        if (StringUtils.isEmpty(s)) {
            return StringUtils.EMPTY;
        }
        if (numberOfChar > s.length()) {
            StringBuilder strBuilder = new StringBuilder(s);
            for (int i = 0; i < (numberOfChar - s.length()); i++) {
                strBuilder.append(c);
            }
            return strBuilder.toString();
        }
        return StringUtils.substring(s, s.length() - numberOfChar, s.length());
    }

    /**
     * parse long
     */
    public static Long parseLong(String string) {
        if (!StringUtils.isEmpty(string) && PATTERN_NUMBER.matcher(string).matches()) {
            return Long.parseLong(string);
        }
        return null;
    }

    /**
     * create file url with file id
     */
    public static String getFileURL(String fileId, String domain, String fileApi) {
        return domain + fileApi + "/" + fileId;
    }

    /**
     * Format phone number: +AAxxxxxxxxxx
     * AA: country code
     * Remove prefix 00
     */
    public static String normalizePhoneNumber(String phoneNumber) {
        if (StringUtils.isEmpty(phoneNumber)) {
            return phoneNumber;
        }
        phoneNumber = phoneNumber.trim().replaceAll("[^0-9]", "");
        phoneNumber = phoneNumber.replaceAll("^[0]*", "");

        if (!phoneNumber.startsWith("+")) {
            phoneNumber = "+" + phoneNumber;
        }
        return phoneNumber;
    }

    /**
     * Format phone number string to INDO_PHONE_NUMBER_FORMAT format
     */
    public static String formatIndoPhoneNumber(String phoneNumber) {
        if (phoneNumber == null) {
            return null;
        }
        phoneNumber = phoneNumber.trim().replaceAll("[^0-9]", "");
        phoneNumber = phoneNumber.replaceAll("^[0]*", "");
        String codePart = phoneNumber.substring(0, 2);
        String phonePart = phoneNumber.substring(2);
        if (StringUtils.equals(codePart, INDO_PHONE_CODE) && StringUtils.length(phonePart) >= 10) {
            //phone number includes code
            return String.format(INDO_PHONE_NUMBER_FORMAT, phonePart);
        } else if (StringUtils.equals(codePart, INDO_HOME_PHONE_CODE) && StringUtils.length(phonePart) >= 10) {
            return String.format(INDO_HOME_PHONE_NUMBER_FORMAT, phonePart);
        } else {
            //phone number not include code
            return String.format(INDO_PHONE_NUMBER_FORMAT, phoneNumber);
        }
    }

    public static boolean isIndoPhoneNumber(String phoneNumber) {
        return StringUtils.isNotEmpty(phoneNumber) && phoneNumber.matches("^[\\+]*62\\d+$");
    }

    public static String removeAllSpecialCharFromString(String s) {
        if (StringUtils.isEmpty(s)) {
            return StringUtils.EMPTY;
        }
        String result = s.replaceAll("[^a-zA-Z0-9]", " ").replaceAll("\\s+", " ");
        return result.trim();
    }


    public static String convertStreamToString(InputStream inputStream) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(inputStream));
            String result = br.lines().collect(Collectors.joining("\n"));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static Boolean checkDobAndKtpMatching(String dob, String ktp) {
        if (StringUtils.isEmpty(dob) || StringUtils.isEmpty(ktp) || ktp.length() < 13) {
            return Boolean.FALSE;
        }
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        Date inputDob = null;
        Date extractDob = null;
        try {
            inputDob = dateFormat1.parse(dob);
        } catch (ParseException ignore) {
            return Boolean.FALSE;
        }
        String inputDobYear = yearFormat.format(inputDob);
        if (StringUtils.isEmpty(inputDobYear) || inputDobYear.length() < 4) {
            return Boolean.FALSE;
        }
        String dobStrFromKtp = ktp.substring(6, 12);
        if (StringUtils.isEmpty(dobStrFromKtp)) {
            return Boolean.FALSE;
        }
        String inputDobYearPrefix = inputDobYear.substring(0, 2);
        String dobStrFromKtpYear = dobStrFromKtp.substring(4);
        String dobStrFromKtpDay = dobStrFromKtp.substring(0, 2);
        String dobStrFromKtpMonth = dobStrFromKtp.substring(2, 4);
        Integer dobIntFromKtpDay = null;
        try {
            dobIntFromKtpDay = Integer.parseInt(dobStrFromKtpDay);
        } catch (NumberFormatException ignore) {
            return Boolean.FALSE;
        }
        if (dobIntFromKtpDay.compareTo(40) > 0) {
            dobIntFromKtpDay -= 40;
        }
        dobStrFromKtp = String.format("%s-%s-%s%s", dobIntFromKtpDay, dobStrFromKtpMonth, inputDobYearPrefix, dobStrFromKtpYear);
        try {
            extractDob = dateFormat.parse(dobStrFromKtp);
        } catch (ParseException ignore) {
            return Boolean.FALSE;
        }
        return extractDob.compareTo(inputDob) == 0;
    }

    public static String hideEmail(String email) {
        return email.replaceAll("(?<=.{2}).(?=[^@]*?.@)", "*");
    }

    public static String hidePhoneNumber(String phoneNumber) {
        return phoneNumber.replaceAll("(?<=.{4}).(?=.*.{2}$)", "*");
    }

    public static String firstLetterCapitalWithSingleSpace(final String words) {
        return Stream.of(words.trim().split("\\s"))
                .filter(word -> word.length() > 0)
                .map(word -> word.substring(0, 1).toUpperCase() + word.substring(1))
                .collect(Collectors.joining(" "));
    }

    public static boolean checkIsIndonesiaPhone(String phoneNumber) {
        if (isBlank(phoneNumber)) {
            return false;
        } else if (phoneNumber.trim().charAt(0) == '+') {
            return phoneNumber.trim().substring(1, 3).equals("62");
        } else {
            return phoneNumber.trim().substring(0, 2).equals("62");
        }
    }

    public static String toHex(String input) {
        StringBuffer buffer = new StringBuffer();
        int intValue;
        for (int x = 0; x < input.length(); x++) {
            int cursor = 0;
            intValue = input.charAt(x);
            String binaryChar = new String(Integer.toBinaryString(input.charAt(x)));
            for (int i = 0; i < binaryChar.length(); i++) {
                if (binaryChar.charAt(i) == '1') {
                    cursor += 1;
                }
            }
            buffer.append(Integer.toHexString(intValue));
        }
        return buffer.toString();
    }

    public static String generateCode(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static String capitalize(String str) {

        // Create a char array of given String
        char ch[] = str.toCharArray();
        for (int i = 0; i < str.length(); i++) {

            // If first character of a word is found
            if (i == 0 && ch[i] != ' ' ||
                    ch[i] != ' ' && ch[i - 1] == ' ') {

                // If it is in lower-case
                if (ch[i] >= 'a' && ch[i] <= 'z') {

                    // Convert into Upper-case
                    ch[i] = (char) (ch[i] - 'a' + 'A');
                }
            }

            // If apart from first character
            // Any one is in Upper-case
            else if (ch[i] >= 'A' && ch[i] <= 'Z')

                // Convert into Lower-Case
                ch[i] = (char) (ch[i] + 'a' - 'A');
        }

        // Convert the char array to equivalent String
        String st = new String(ch);
        return st;
    }

    public static List<String> parseCSV(MultipartFile file) {
        List<String> list = new ArrayList<>();
        if (!file.isEmpty()) {
            try {
                InputStream inputStream = file.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                list = br.lines().collect(Collectors.toList());

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return list;
    }

    /*
     * Convert String to timestamp
     * String format yyyy-MM-dd
     * */
    public static Timestamp toTimesTamp(String date) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parsedDate = dateFormat.parse(date);
            timestamp = new Timestamp(parsedDate.getTime());
        } catch (Exception e) { //this generic but you can control another types of exception
            // look the origin of excption
            e.printStackTrace();
        }
        return timestamp;
    }

    /*
     * set value null if empty string
     */
    public static String valueOn(String string) {
        if (StringUtils.isEmpty(string))
            return null;

        return string;
    }

    public static String formatDecimal(Object data) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.forLanguageTag("ID"));
        DecimalFormat decimalFormat = (DecimalFormat) nf;
        return decimalFormat.format(data);
    }

    public static BigDecimal convertTo4Decimal(BigDecimal data) {
        if (data == null) {
            return null;
        }
        return data.setScale(4, RoundingMode.HALF_DOWN);
    }

    public static String convertBigDecimalToString(BigDecimal data) {
        if (data == null) return "";
        return data.toString();
    }

    public static String validateBigDecimal(BigDecimal value, String valueName, Integer precision, Integer scale) {
        String notes = "";
        if (value == null) {
            notes = "tidak boleh kosong, jika tidak ada value isikan 0 (nol)";
        } else {
            if (value.compareTo(BigDecimal.ZERO) < 0)
                notes = "Karakter " + valueName + " tidak boleh minus\n";

            if (value.precision() > precision)
                notes += "Total digit karakter " + valueName + " tidak boleh lebih dari " + precision + "\n";

            if (value.scale() > scale)
                notes += "Karakter " + valueName + " desimal tidak boleh lebih dari " + scale + "\n";

        }

        return notes;
    }

    public static String wrongParseNumber(String data, String name) throws Exception {
        if (data == null || data.isEmpty() || data.equals("-") || data.equalsIgnoreCase("null"))
            return "0";

        String regex = "^-?[0-9]\\d*(\\.\\d+)?$";
        if (!Pattern.matches(regex, data)) {
            throw new Exception(name + " hanya dapat input angka ");
        }
        return data;
    }

    /**
     * just for testing
     *
     * @param args
     */
    public static void main(String... args) throws Exception {
        int nextDay = 0;
        Calendar fromEndDate = Calendar.getInstance();
        fromEndDate.add(Calendar.DATE, nextDay);
        fromEndDate.set(Calendar.HOUR_OF_DAY, 0);
        fromEndDate.set(Calendar.MINUTE, 0);
        fromEndDate.set(Calendar.SECOND, 0);
        fromEndDate.set(Calendar.MILLISECOND, 0);
        System.out.println(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(fromEndDate.getTime()));

        Calendar.getInstance().clear();
        Calendar toEndDate = Calendar.getInstance();
        toEndDate.add(Calendar.DATE, nextDay);
        toEndDate.set(Calendar.HOUR_OF_DAY, 23);
        toEndDate.set(Calendar.MINUTE, 59);
        toEndDate.set(Calendar.SECOND, 59);
        toEndDate.set(Calendar.MILLISECOND, 999);
        System.out.println(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(toEndDate.getTime()));
        Calendar.getInstance().clear();

        Date endDate = DateUtils.parseDate("02/01/2020", DateUtils.DD_MM_YYYY);
        System.out.println(new Date().after(endDate));

        System.out.println(StringUtils.validateBigDecimal(BigDecimal.valueOf(Double.parseDouble("12345")), "double", 5, 3));
        System.out.println(StringUtils.validateBigDecimal(new BigDecimal("12345.90"), "integer", 5, 3));
        System.out.println(StringUtils.validateBigDecimal(new BigDecimal("-123.89"), "new", 5, 3));
        System.out.println(StringUtils.wrongParseNumber("0.09", "test"));

    }
}
