package springboot.core.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.core.common.GenericSpesification;
import springboot.core.common.SearchCriteria;
import springboot.core.common.SearchOperation;
import springboot.core.model.dto.ActivityGenericLog;
import springboot.core.model.entity.ActivityLog;
import springboot.core.model.entity.LoginLog;
import springboot.core.repository.ActivityLogRepository;
import springboot.core.repository.LoginLogRepository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;

/**
 * Created by f.putra on 1/12/21.
 */
@Service
public class LogServices extends BaseCriteriaBuilder {
    @Autowired
    ActivityLogRepository activityLogRepository;
    @Autowired
    LoginLogRepository loginLogRepository;

    public void saveActivityLog(ActivityLog v) {
        activityLogRepository.save(v);
    }

    public void saveGeneralActivityLog(ActivityGenericLog activityGenericLog) {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            String jsonBefore = ow.writeValueAsString(activityGenericLog.getBefore());
            String jsonAfter = ow.writeValueAsString(activityGenericLog.getAfter());

            ActivityLog activityLog = new ActivityLog();
            activityLog.setACTION(activityGenericLog.getBefore() == null ? "SAVE" : "UPDATE");
            activityLog.setTABLE_NAME(activityGenericLog.getTableName());
            activityLog.setOLD_DATA(jsonBefore);
            activityLog.setNEW_DATA(jsonAfter);
            activityLogRepository.save(activityLog);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public void saveLoginLog(LoginLog v) {
        loginLogRepository.save(v);
    }

    @Transactional
    public void updateLoginLog(LoginLog v) {
        LoginLog loginLog = loginLogRepository.findUser(v.getUSERNAME()).orElseThrow();
        loginLog.setTIME_LOGOUT(new Date());
    }

    @Transactional(readOnly = true)
    public Page<ActivityLog> listActivityLog(Pageable pageable, ActivityLog v) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ActivityLog> query = builder.createQuery(ActivityLog.class);
        Root<ActivityLog> rootActivityLog = query.from(ActivityLog.class);
        applyOrderByCreatedAndUpdateDate(builder, rootActivityLog, query);

        GenericSpesification<ActivityLog> genericSpesification = new GenericSpesification<>();
        if (v.getTABLE_NAME() != null) {
            genericSpesification.add(new SearchCriteria(rootActivityLog.get("TABLE_NAME"), v.getTABLE_NAME(), SearchOperation.MATCH));
        }
        if (v.getCreatedBy() != null) {
            genericSpesification.add(new SearchCriteria(rootActivityLog.get("createdBy"), v.getCreatedBy(), SearchOperation.MATCH));
        }
        query.where(genericSpesification.toPredicate(rootActivityLog, query, builder));

        TypedQuery<ActivityLog> queryList = entityManager.createQuery(query)
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize());

        long totalRows = activityLogRepository.count(genericSpesification);

        try {
            return new PageImpl<>(queryList.getResultList(), pageable, totalRows);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Transactional(readOnly = true)
    public Page<LoginLog> listLoginLog(Pageable pageable, LoginLog v) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<LoginLog> query = builder.createQuery(LoginLog.class);
        Root<LoginLog> rootLoginLog = query.from(LoginLog.class);
        applyOrderByCreatedAndUpdateDate(builder, rootLoginLog, query);

        GenericSpesification<LoginLog> genericSpesification = new GenericSpesification<>();
        if (v.getUSERNAME() != null) {
            genericSpesification.add(new SearchCriteria(rootLoginLog.get("USERNAME"), v.getUSERNAME(), SearchOperation.MATCH));
        }
        if (v.getROLE() != null) {
            genericSpesification.add(new SearchCriteria(rootLoginLog.get("ROLE"), v.getROLE(), SearchOperation.MATCH));
        }
        if (v.getPLATFORM() != null) {
            genericSpesification.add(new SearchCriteria(rootLoginLog.get("PLATFORM"), v.getPLATFORM(), SearchOperation.MATCH));
        }
        if (v.getLOCATION() != null) {
            genericSpesification.add(new SearchCriteria(rootLoginLog.get("LOCATION"), v.getLOCATION(), SearchOperation.MATCH));
        }
        query.where(genericSpesification.toPredicate(rootLoginLog, query, builder));

        TypedQuery<LoginLog> queryList = entityManager.createQuery(query)
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize());

        long totalRows = loginLogRepository.count(genericSpesification);

        try {
            return new PageImpl<>(queryList.getResultList(), pageable, totalRows);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
