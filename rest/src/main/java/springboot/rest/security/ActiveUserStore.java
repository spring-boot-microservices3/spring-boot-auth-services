package springboot.rest.security;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by f.putra on 12/04/18.
 */
public class ActiveUserStore implements Serializable {

    public List<String> users;

    public ActiveUserStore() {
        users = new ArrayList<String>();
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }
}
