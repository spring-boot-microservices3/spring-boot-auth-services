package springboot.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import springboot.core.model.entity.MasterMenu;

import java.util.Set;

public interface MasterMenuRepository extends BaseRepository<MasterMenu, String> {

    @Query("SELECT mm FROM MasterMenu mm WHERE mm.name LIKE %:name%")
    Set<MasterMenu> searchMenuByName(@Param("name") String name);

    Boolean existsByNameIgnoreCase(String name);

    Boolean existsMasterMenuByPath(String name);

    MasterMenu findByName(String name);
}
