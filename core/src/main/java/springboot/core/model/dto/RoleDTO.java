package springboot.core.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by f.putra on 9/24/20.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDTO extends BaseDTO implements Serializable {

    private static final long serialVersionUID = -8940437655236019736L;

    private String name;
    private Integer level;
    private String description;
}
