package springboot.rest.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springboot.core.common.ResourceBundle;
import springboot.core.common.StringUtils;
import springboot.core.model.dto.UserDTO;
import springboot.core.services.UserServices;
import springboot.rest.exception.RestClientException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by f.putra on 9/24/20.
 */
@RestController
public class MasterUserController extends BaseController {
    @Autowired
    UserServices userServices;

    @Autowired
    ResourceBundle resourceBundle;

    @ApiOperation(value = "Register User", produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UserDTO userDTO) throws RestClientException {
        if (userDTO.getEmail() == null || StringUtils.isEmpty(userDTO.getEmail()))
            throw new RestClientException(HttpStatus.BAD_REQUEST, resourceBundle.getMessage("err.user.validation.email"));
        if (userDTO.getUsername() == null || StringUtils.isEmpty(userDTO.getUsername()))
            throw new RestClientException(HttpStatus.BAD_REQUEST, resourceBundle.getMessage("err.user.validation.username"));
        try {
            return getCallBack(userServices.registerUser(userDTO), HttpStatus.OK);
        } catch (Exception e) {
            return getCallBack(e, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Update User", produces = MediaType.APPLICATION_JSON_VALUE)
    @PutMapping("/user/update/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") String id, @RequestBody UserDTO userDTO) {
        try {
            return getCallBack(userServices.updateUser(id, userDTO), HttpStatus.OK);
        } catch (Exception e) {
            return getCallBack(e, HttpStatus.BAD_REQUEST);
        }
    }

    //    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Delete User", produces = MediaType.APPLICATION_JSON_VALUE)
    @DeleteMapping("/user/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") String id) {
        try {
            return getCallBack(userServices.deleteUser(id), HttpStatus.OK);
        } catch (Exception e) {
            return getCallBack(e, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Get User Profile", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/profile")
    public ResponseEntity<?> getUserProfile() {
        return getCallBack(userServices.getUserProfile(), HttpStatus.OK);
    }

    @ApiOperation(value = "Get All User", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/user/list")
    public ResponseEntity<?> getUserList(@RequestParam(name = "offset") Integer offset,
                                         @RequestParam(name = "limit") Integer limit,
                                         @RequestParam(name = "fullname", required = false) String fullname,
                                         @RequestParam(name = "role", required = false) String role,
                                         @RequestParam(name = "isDelete", required = false) Boolean isDelete
    ) throws ParseException {
        UserDTO user = new UserDTO();
        user.setFullname(fullname);
        user.setRole(role);
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        user.setIsDelete(isDelete);
        return getCallBack(userServices.getAllUser(PageRequest.of(offset, limit), user), HttpStatus.OK);
    }
}
