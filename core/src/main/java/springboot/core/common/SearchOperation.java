package springboot.core.common;

/**
 * Created by f.putra on 10/20/20.
 */
public enum SearchOperation {
    GREATER_THAN,
    LESS_THAN,
    GREATER_THAN_EQUAL,
    LESS_THAN_EQUAL,
    NOT_EQUAL,
    EQUAL,
    MATCH,
    MATCH_INTEGER,
    MATCH_END
}
