package springboot.core.model.dto.response;

import lombok.Value;

import java.util.Date;

@Value
public class UserResponse {
    String userId;
    String username;
    String email;
    boolean isActive;
    RoleResponse role;
    Date lastLoginAt;
}
