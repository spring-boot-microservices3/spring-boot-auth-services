package springboot.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springboot.core.model.dto.RoleDTO;
import springboot.core.services.RoleServices;

/**
 * Created by f.putra on 9/24/20.
 */
@RestController
@RequestMapping("role")
public class MasterRoleController extends BaseController {

    @Autowired
    RoleServices roleServices;

    @PostMapping("/create")
    public ResponseEntity<?> createRole(@RequestBody RoleDTO payloadRole) {
        try{
        return getCallBack(roleServices.saveRole(payloadRole), HttpStatus.OK);
        }catch (Exception e){
            return getCallBack(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateRole(@RequestBody RoleDTO payloadRole) {
        try{
        return getCallBack(roleServices.updateRole(payloadRole), HttpStatus.OK);
        }catch (Exception e){
            return getCallBack(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/list")
    public ResponseEntity<?> getRoleList(@RequestParam(name = "offset") Integer offset,
                                           @RequestParam(name = "limit") Integer limit,
                                           @RequestParam(name = "name", required = false) String name,
                                           @RequestParam(name = "level", required = false) Integer level) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setName(name);
        roleDTO.setLevel(level);
        return getCallBack(roleServices.findAllRole(PageRequest.of(offset, limit), roleDTO), HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<?> getMenuList(@RequestParam(name = "name") String name) {
        return getCallBack(roleServices.searchRole(name), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteRole(@PathVariable(name = "id") String id) {
        return getCallBack(roleServices.deleteRole(id), HttpStatus.OK);
    }
}
