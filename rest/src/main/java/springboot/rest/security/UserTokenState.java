package springboot.rest.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import springboot.core.model.dto.response.UserResponse;

/**
 * Created by f.putra on 2/23/20.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserTokenState {

    private UserResponse user;
    private String access_token;
    private HttpStatus status;
}
