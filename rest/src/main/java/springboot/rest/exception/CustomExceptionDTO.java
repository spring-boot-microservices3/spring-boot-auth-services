package springboot.rest.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomExceptionDTO {
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Exception description
   */
  private String message;
  /**
   * Resource bundle code, error code
   */
  private String code;
  /**
   * Log trace id
   */
  private String traceId;

  public CustomExceptionDTO(String message, String code){
    this(message, code, null);
  }

  public CustomExceptionDTO(String message, String code, String traceId){
    this.message = message;
    this.code = code;
    this.traceId = traceId;
  }

}
