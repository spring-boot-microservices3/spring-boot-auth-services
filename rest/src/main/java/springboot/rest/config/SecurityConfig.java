package springboot.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import springboot.core.services.UserServices;
import springboot.rest.security.ActiveUserStore;
import springboot.rest.security.TokenAuthenticationFilter;
import springboot.rest.security.handler.CustomAuthenticationEntryPoint;
import springboot.rest.security.handler.LogoutHandler;
import springboot.rest.security.helper.TokenHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;

/**
 * Created by f.putra on 23/11/19.
 */
@Configuration
@ComponentScan(basePackages = {"springboot.core", "springboot.rest.security"})
@EnableWebSecurity
@Order(SecurityProperties.BASIC_AUTH_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserServices userServices;
    @Autowired
    TokenHelper tokenHelper;

    private static final String SQL_LOGIN
            = "select p.username, p.hashed_password as password, true as enabled " +
            "from pengguna p where p.username = ?";

    private static final String SQL_ROLE
            = "select p.username, r.nama as authority from s_role r " +
            "inner join pengguna p on p.id_role = r.id "
            + "where p.username = ?";

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(userServices)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorization = http.authorizeRequests();

        // We don't need CSRF
        http.csrf().disable()
                .formLogin().disable();
        // When the user has logged in as XX.
        // But access a page that requires role YY,
        // AccessDeniedException will be thrown.
        // http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403");

        // The pages does not require login

        authorization.antMatchers("/swagger-ui/**", "/swagger-ui.html", "/swagger-resources/**", "/v2/api-docs/**", "/webjars/springfox-swagger-ui/**").permitAll();

        authorization.antMatchers(
                "/login",
                "/user-logout",
                "/parameter/find/group",
                "/log/**",
                "/*"
        ).permitAll();

        // make sure we use stateless session; session won't be used to
        // store user's state.
        http.cors().configurationSource(this::getCorsConfiguration)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .sessionFixation().migrateSession()
                .maximumSessions(1)
                .maxSessionsPreventsLogin(true)
                .expiredUrl("/expired")
                .sessionRegistry(sessionRegistry());

//        http.authorizeRequests(authorize -> authorize
//                .mvcMatchers("/switchuser/exit")
//                .hasAuthority(SwitchUserFilter.ROLE_PREVIOUS_ADMINISTRATOR)
//                .mvcMatchers("/switchuser/select", "/switchuser/form")
//                .hasAuthority("Administrator")
//                .anyRequest().authenticated())
//                .addFilterAfter(switchUserFilter(), FilterSecurityInterceptor.class);

        // all other requests need to be authenticated
        authorization.anyRequest().authenticated().and().
                exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
                .and()
                .authorizeRequests().filterSecurityInterceptorOncePerRequest(true)
                .and()
                // Add a filter to validate the tokens with every request
                .addFilterBefore(new TokenAuthenticationFilter(tokenHelper, userServices), BasicAuthenticationFilter.class)
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessHandler(new LogoutHandler()).deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .logoutSuccessUrl("/login");
    }

    @Bean
    public SwitchUserFilter switchUserFilter() throws Exception {
        SwitchUserFilter filter = new SwitchUserFilter();
        filter.setUserDetailsService(userDetailsService());
        filter.setSwitchUserUrl("/switchuser/form");
        filter.setExitUserUrl("/switchuser/exit");
        filter.setTargetUrl("/**");
        return filter;
    }

    private CorsConfiguration getCorsConfiguration(final HttpServletRequest request) {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedHeaders(Collections.singletonList("*"));
        config.setAllowedMethods(Collections.singletonList("*"));
        config.addAllowedOrigin(request.getHeader("Origin"));
        config.setAllowCredentials(true);
        return config;
    }

    @Bean
    public ActiveUserStore activeUserStore() {
        return new ActiveUserStore();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new CustomAuthenticationEntryPoint();
    }
}
