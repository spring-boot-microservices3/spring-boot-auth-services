package springboot.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * Created by f.putra on 26/02/21.
 */
@SpringBootApplication(scanBasePackages = {"springboot.core", "springboot.rest"}, exclude = {SecurityAutoConfiguration.class})
public class App extends SpringBootServletInitializer {
    public static Logger getLogger(Object o) {
        return LogManager.getLogger(o.getClass());
    }

    public static void main(String[] args) {
        try {
            SpringApplication.run(App.class, args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostConstruct
    void started() {
        // set JVM timezone as UTC
        TimeZone.setDefault(TimeZone.getTimeZone("Bangkok/Jakarta"));
    }
}
