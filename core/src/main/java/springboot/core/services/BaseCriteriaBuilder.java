package springboot.core.services;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by f.putra on 01/04/18.
 * <p>
 * bisa dimodifikasi lebih lanjut, entity manager wajib persistence tidak inject dengan @Autowire
 */
public abstract class BaseCriteriaBuilder<T> {

    @PersistenceContext(name = "tap")
    protected EntityManager entityManager;

    public void applyOrderByCreatedAndUpdateDate(CriteriaBuilder builder, Root<T> root, CriteriaQuery<T> query) {
        //In the class code
        //private static final Date MIN_DATE = new Date(0L);
        final Date MIN_DATE = new Date();

        final Date MIN_DATE_UPDATE = new Date(0L);

        List<Order> orderList = new ArrayList<>();
        orderList.add(builder.desc(builder.selectCase()
                .when(builder.greaterThan(root.get("createdDtm"), root.get("updateDtm")), root.get("createdDtm"))
                .otherwise(root.get("updateDtm"))));

        //We treat records will NULL dateTimeField as if it was MIN_DATE.
        Order createdDateDescOrder = builder.desc(
                //NULL values - last - WORKAROUND.
                builder.coalesce(root.get("createdDtm"), MIN_DATE));

        Order updatedDateDescOrder = builder.desc(
                //NULL values - last - WORKAROUND.
                builder.coalesce(root.get("updateDtm"), MIN_DATE_UPDATE));
        query.orderBy(orderList);
    }
}
