package springboot.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springboot.core.model.entity.ActivityLog;
import springboot.core.model.entity.LoginLog;
import springboot.core.services.LogServices;

/**
 * Created by f.putra on 10/8/20.
 */
@RestController
@RequestMapping("log")
public class LogController extends BaseController {

    @Autowired
    LogServices logServices;

    @GetMapping("/activity-log")
    public ResponseEntity<?> listActivityLog(@RequestParam(name = "offset") Integer offset,
                                             @RequestParam(name = "limit") Integer limit,
                                             @RequestParam(name = "username", required = false) String username,
                                             @RequestParam(name = "table_name", required = false) String table_name) {
        ActivityLog activityLog = new ActivityLog();
        activityLog.setTABLE_NAME(table_name);
        activityLog.setCreatedBy(username);
        return getCallBack(logServices.listActivityLog(PageRequest.of(offset, limit), activityLog), HttpStatus.OK);
    }

    @GetMapping("/login-log")
    public ResponseEntity<?> listLoginLog(@RequestParam(name = "offset") Integer offset,
                                          @RequestParam(name = "limit") Integer limit,
                                          @RequestParam(name = "username", required = false) String username,
                                          @RequestParam(name = "role", required = false) String role,
                                          @RequestParam(name = "platform", required = false) String platform,
                                          @RequestParam(name = "lokasi", required = false) String lokasi) {
        LoginLog loginLog = new LoginLog();
        loginLog.setUSERNAME(username);
        loginLog.setROLE(role);
        loginLog.setPLATFORM(platform);
        loginLog.setLOCATION(lokasi);
        return getCallBack(logServices.listLoginLog(PageRequest.of(offset, limit), loginLog), HttpStatus.OK);
    }
}
