package springboot.core.common;

import org.springframework.stereotype.Component;

/**
 * Created by f.putra on 22/11/19.
 *  * Constant will used on the backend core.service for message or constant value core.test.service
 */
@Component
public class Constant {

    public static String toTitleCase(String givenString) {
        String initialString = givenString.toLowerCase();
        String[] arr = initialString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }
}
