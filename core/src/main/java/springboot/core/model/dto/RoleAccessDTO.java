package springboot.core.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by f.putra on 10/7/20.
 */
@AllArgsConstructor
@Getter
@Setter
public class RoleAccessDTO implements Serializable {

    private static final long serialVersionUID = 63054562864934251L;

    private String menu_role_id;
    private String menu_id;
    private String role_id;
    private String name;
    private String icon;
    private String path;
    private String role_name;
    private String parent;
    private String parent_name;
    private int level;
    private Boolean is_create;
    private Boolean is_update;
    private Boolean is_read;
    private Boolean is_delete;
    private Boolean is_active;
}
