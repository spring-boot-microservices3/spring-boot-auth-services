package springboot.core.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AddRoleDTO implements Serializable {
    private static final long serialVersionUID = -2482691114447794177L;

    private String userId;
    private List<String> roleId;
    private List<String> menus;
}
