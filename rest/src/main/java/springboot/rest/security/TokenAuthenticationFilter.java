package springboot.rest.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import springboot.core.common.UserPrincipal;
import springboot.core.services.UserServices;
import springboot.rest.security.helper.TokenBasedAuthentication;
import springboot.rest.security.helper.TokenHelper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TokenAuthenticationFilter extends OncePerRequestFilter {

    private TokenHelper tokenHelper;

    private UserServices userServices;

    public TokenAuthenticationFilter(TokenHelper tokenHelper, UserServices userServices) {
        this.tokenHelper = tokenHelper;
        this.userServices = userServices;
    }


    @Override
    public void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain
    ) throws IOException, ServletException {
        String username;
        String authToken = tokenHelper.getToken(request);
        if (authToken != null) {
            // get username from token
            username = tokenHelper.getUsernameFromToken(authToken);
            if (username != null) {
                // get user
                UserPrincipal userDetails = userServices.loadUserByUsername(username);
                if (tokenHelper.validateToken(authToken, userDetails)) {
                    // create autTokenBasedAuthenticationhentication
                    TokenBasedAuthentication authentication = new TokenBasedAuthentication(userDetails);
                    authentication.setToken(authToken);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                } else {
                    throw new IllegalArgumentException("Expired JWT token "  + userDetails.getUsername());
                }
            }
        }

        chain.doFilter(request, response);
    }

}
