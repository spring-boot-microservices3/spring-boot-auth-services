package springboot.core.model.dto;

import lombok.*;

import java.io.Serializable;

/**
 * Created by f.putra on 1/29/21.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ActivityGenericLog implements Serializable {

    private static final long serialVersionUID = 1621214441385700149L;

    private Object before;
    private Object after;
    private String tableName;
}
