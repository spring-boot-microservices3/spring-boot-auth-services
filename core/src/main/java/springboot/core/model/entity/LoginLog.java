package springboot.core.model.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by f.putra on 1/12/21.
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "LOG_LOGIN")
public class LoginLog extends Auditable<String> implements Serializable {

    private static final long serialVersionUID = 5036700383701976167L;
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    private String id;
    @Column(name = "USERNAME")
    private String USERNAME;
    @Column(name = "ROLE")
    private String ROLE;
    @Column(name = "LOCATION")
    private String LOCATION;
    @Column(name = "PLATFORM")
    private String PLATFORM;
    @Column(name = "TIME_LOGIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date TIME_LOGIN;
    @Column(name = "TIME_LOGOUT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date TIME_LOGOUT;
    @Column(name = "DESCRIPTION")
    private String DESCRIPTION;
}
