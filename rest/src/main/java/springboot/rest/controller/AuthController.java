package springboot.rest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import springboot.core.common.ContextHolder;
import springboot.core.common.DateUtils;
import springboot.core.common.UserPrincipal;
import springboot.core.model.dto.UserDTO;
import springboot.core.model.dto.response.UserResponse;
import springboot.core.model.entity.LoginLog;
import springboot.core.repository.MasterUserRepository;
import springboot.core.services.LogServices;
import springboot.core.services.UserServices;
import springboot.rest.exception.RestClientException;
import springboot.rest.security.UserTokenState;
import springboot.rest.security.dto.SupportingAuthParameter;
import springboot.rest.security.helper.TokenHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@RestController
public class AuthController extends BaseController {
    @Autowired
    UserServices userServices;
    @Autowired
    MasterUserRepository masterUserRepository;
    @Autowired
    SessionRegistry sessionRegistry;
    @Autowired
    TokenHelper tokenHelper;
    @Autowired
    LogServices logServices;
    @Autowired
    AuthenticationManager authenticationManager;

    /**
     * login user HTTP BASIC authentication headers (an IETF RFC-based standard)
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody SupportingAuthParameter parameter,
                                                       HttpServletRequest request,
                                                       HttpServletResponse response) throws RestClientException, JsonProcessingException {
        final Date loginAt = DateUtils.nowTimestamp();


        Boolean existUser = masterUserRepository.existsByUsername(parameter.getUsername());
        if (!existUser)
            throw new RestClientException(HttpStatus.NOT_ACCEPTABLE, resourceBundle.getMessage("err.user.validation.not_registered"));

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(parameter.getUsername(), parameter.getPassword()));

        //             Inject into security context
        SecurityContextHolder.getContext().setAuthentication(authentication);

        //             token creation
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        String jws = tokenHelper.generateToken(principal.getUsername(), loginAt);

        //     validate only 1 user can login each account
//        MasterUser user = userServices.getUserById(principal.getId());
//        if (user.getLastLoginAt() != null)
//            throw new RestClientException(HttpStatus.FORBIDDEN, resourceBundle.getMessage("err.user.validation.already_login", user.getUsername()));

        // get menu after login
        UserResponse userResponse = userServices.getMenuRoleAfterLogin(principal.getId());

        /**
         *  log user login
         *
         */
        logServices.saveLoginLog(new LoginLog(null, principal.getUsername(), userResponse.getRole().getName(), null, request.getHeader("User-Agent"), loginAt, null, "LOGIN"));
//        userServices.saveLastLogin(principal.getId(), loginAt);

        // Return the token
        return getCallBack(new UserTokenState(userResponse, jws, HttpStatus.OK), HttpStatus.OK);
    }

    @PostMapping(value = "/user-logout")
    public String logout(@RequestBody UserDTO userDTO,
                         HttpServletRequest request,
                         HttpServletResponse response) {
        UserPrincipal principal = ContextHolder.getRequestAuthor();
        userServices.logout(principal.getId());
        return "redirect:/logout";
    }
}
