package springboot.core.model.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by f.putra on 9/8/20.
 */
@Getter
@Setter
@ToString
@Entity
@Table(name = "t_user_role")
public class UserRole extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "user_role_id")
    private String userRoleId;

    @Column(name = "role_id")
    private String roleId;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "is_active")
    private boolean isActive;

    @OneToOne
    @PrimaryKeyJoinColumn(name="user_id", referencedColumnName="user_id")
    private MasterUser masterUser;

    @OneToOne(fetch = FetchType.EAGER)
    @PrimaryKeyJoinColumn(name="role_id", referencedColumnName="role_id")
    private MasterRole masterRole;
}
