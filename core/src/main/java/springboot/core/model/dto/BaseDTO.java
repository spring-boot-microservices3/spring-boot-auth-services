package springboot.core.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by f.putra on 10/8/20.
 */
@Getter
@Setter
public class BaseDTO {

    private String id;
    private Boolean isDelete;

    @JsonIgnore
    private String notes;
}
